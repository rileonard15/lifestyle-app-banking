from django.conf.urls import include, url
from django.contrib import admin


urlpatterns = [
    url(r'^api/v1/', include('apiv1.urls')),
    url(r'^mobile/apiv1/', include('apiv1.urls')),
    url(r'^web/', include('main.urls')),

    url(r'^admin/', admin.site.urls),
    url(r'^', include('front.urls')),
]
