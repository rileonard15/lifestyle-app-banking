from main.base import BaseHandler
from apiv1.models import Token, BankAccounts, MyAccount, Feeds, MySubscription
# from rest_framework.authtoken.models import Token
from apiv1.serializers import BankAccountsSerializer, GetBankAccountsSerializer, UserSerializer, AccountSerializer, FeedsSerializer, GetFeedsSerializer, MySubscriptionSerializer, GetMySubscriptionSerializer, MerchantSerializer
from main import helpers as h
from front.models import Merchant, Package
from main.handlers.subscription import PackageSerializer
from django.contrib.auth.models import User
from django.contrib.auth import models


import uuid
import urllib
import urllib2
import ast
# from rest_framework.authentication import SessionAuthentication, BasicAuthentication, TokenAuthentication
# from rest_framework.permissions import IsAuthenticated


class BankAccountHandler(BaseHandler):
    def post(self, request):

        # token = Token.objects.get(key=request.data["token"])
        user_id = self.request.user.id
        try:
            acct = BankAccounts.objects.get(user=int(user_id))
            print acct
            serializer = GetBankAccountsSerializer(data=[acct], many=True)
            serializer.is_valid()
            return self.api_response(200, serializer.data[0])
        except:
            data = dict()
            data["user"] = int(user_id)
            data["refid"] = h.generate_code(size=10)

            serializer = BankAccountsSerializer(data=data)
            serializer.is_valid()
            print serializer.errors
            serializer.save()

            return self.api_response(200, serializer.data)


class MobilexLoginHandler(BaseHandler):
    def post(self, request):
        user = User.objects.get(id=self.request.user.id)
        serializer = UserSerializer(data=[user], many=True)
        serializer.is_valid()

        return self.api_response(200, serializer.data[0])

    # authentication_classes = (SessionAuthentication, BasicAuthentication)
    # permission_classes = (IsAuthenticated,)

    # def get(self, request):
    #     return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)

    # def post(self, request, format=None):
    #     token = Token.objects.get_or_create(user=request.user)
    #     print wasad
    #     content = {
    #         'email': unicode(request.user),
    #         'token': token[0].key,
    #         'group': request.user.groups.all()[0].name
    #     }
    #     return Response(content)


class MyAccountHandler(BaseHandler):
    def get(self, request):
        try:
            account = MyAccount.objects.get(user=self.request.user.id)
        except MyAccount.DoesNotExist:
            return self.api_response(200, 0)

        serializer = AccountSerializer(data=[account], many=True)
        serializer.is_valid()
        return self.api_response(200, serializer.data[0])


class UnionBankBranches(BaseHandler):
    def get(self, request):
        url = "http://apidev.unionbankph.com/api/branches"
        req = urllib2.Request(url)
        response = urllib2.urlopen(req)
        result = response.read()

        result = ast.literal_eval(result)
        return self.api_response(200, result)


class UnionBankAccounts(BaseHandler):
    def get(self, request):
        url = "http://apidev.unionbankph.com/api/accounts?account_no=000000000048" 
        req = urllib2.Request(url)
        response = urllib2.urlopen(req)
        result = response.read()
        result = result.replace("null", '""')
        result = ast.literal_eval(result)
        return self.api_response(200, result)

class FeedsApiHandler(BaseHandler):
    def get(self, request):
        feeds = Feeds.objects.all()
        serializer = GetFeedsSerializer(data=feeds, many=True)
        serializer.is_valid()
        serializer.save()

        return self.api_response(200, serializer.data)

    def post(self, request):
        self.request.data["user"] = self.request.user.id
        serializer = FeedsSerializer(data=self.request.data)
        serializer.is_valid()
        serializer.save()

        return self.api_response(200, serializer.data)

class SubscriptionHandler(BaseHandler):
    def get(self, request):
        subscriptions = MySubscription.objects.filter(user=self.request.user.id)
        serializer = GetMySubscriptionSerializer(data=subscriptions, many=True)
        serializer.is_valid()
        return self.api_response(200, serializer.data)

    def post(self, request):
        self.request.data["user"] = self.request.user.id

        try:
            subscription = MySubscription.objects.get(user=self.request.data["user"], merchant=self.request.data["merchant"])
            return self.api_response(200, "Already a member")
        except:
            print ""

        serializer = MySubscriptionSerializer(data=self.request.data)

        serializer.is_valid()
        try:
            serializer.save()
        except:
            return self.api_response(400, "Merchant not exist")    
        return self.api_response(200, serializer.data)

class MerchantsHandler(BaseHandler):
    def get(self, request):
        merchants = Merchant.objects.all()
        serializer = MerchantSerializer(data=merchants, many=True)
        serializer.is_valid()

        return self.api_response(200, serializer.data)

class PackagesHandler(BaseHandler):
    def post(self, request):
        print self.request.data
        packages = Package.objects.filter(merchant=int(self.request.data["uid"]))
        serializer = PackageSerializer(data=packages, many=True)
        serializer.is_valid()

        return self.api_response(200, serializer.data)


        





