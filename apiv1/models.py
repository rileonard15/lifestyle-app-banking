from __future__ import unicode_literals
from django.contrib.auth.models import User
from django.db import models
from front.models import Merchant


class BankAccounts(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    refid = models.CharField(max_length=50)
    user = models.OneToOneField(User)

class Token(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    key = models.CharField(max_length=200)
    user = models.OneToOneField(User)

class MyAccount(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    balance = models.FloatField(default=1500)
    user = models.OneToOneField(User)

    def __unicode__(self):
        return self.user.first_name + " " + self.user.last_name

class Feeds(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    content = models.TextField()
    user = models.ForeignKey(User)
    tag = models.CharField(max_length=200)

class MySubscription(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    merchant = models.ForeignKey(User, related_name="MERCHANT")
    user = models.ForeignKey(User, related_name="CUSTOMER")
    
