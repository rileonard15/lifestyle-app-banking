from rest_framework import serializers
from django.contrib.auth.models import User
from apiv1.models import BankAccounts, MyAccount, Feeds, MySubscription
from front.models import Merchant, Company


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username', 'email', 'first_name', 'last_name')

class CompanySerializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = ('id','industry', 'logo', 'name', 'address', 'contact_no', 'authorized', 'user', 'reference_no')


class UserSerializer2(serializers.ModelSerializer):
    company = CompanySerializer()
    class Meta:
        model = User
        fields = ('id', 'username', 'email', 'first_name', 'last_name', 'company')


class BankAccountsSerializer(serializers.ModelSerializer):
    class Meta:
        model = BankAccounts
        fields = ('created', 'refid', 'user')


class GetBankAccountsSerializer(serializers.ModelSerializer):
    user = UserSerializer()
    class Meta:
        model = BankAccounts
        fields = ('created', 'refid', 'user')

class AccountSerializer(serializers.ModelSerializer):
    user = UserSerializer()
    class Meta:
        model = MyAccount
        fields = ('created', 'balance', 'user')

class FeedsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Feeds
        fields = ('created', 'content', 'user', 'tag',)

class GetFeedsSerializer(serializers.ModelSerializer):
    user = UserSerializer()
    class Meta:
        model = Feeds
        fields = ('created', 'content', 'user', 'tag',)


class GetMySubscriptionSerializer(serializers.ModelSerializer):
    merchant = UserSerializer()
    user = UserSerializer()

    class Meta:
        model = MySubscription
        fields = ('created', 'merchant', 'user',)

class MySubscriptionSerializer(serializers.ModelSerializer):
    class Meta:
        model = MySubscription
        fields = ('created', 'merchant', 'user',)

class MerchantSerializer(serializers.ModelSerializer):
    user = UserSerializer2()
    class Meta:
        model = Merchant
        fields = ('account_status', 'user', 'bank')




