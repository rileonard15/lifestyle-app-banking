from django.conf.urls import url

from . import api


urlpatterns = [
    url(r'^vlogin', api.MobilexLoginHandler.as_view(), name='mobile-login'),
    url(r'^subscription/', api.SubscriptionHandler.as_view(), name='merchant-subscribe'),
    url(r'^merchants/', api.MerchantsHandler.as_view(), name='merchants'),
    url(r'^packages/', api.PackagesHandler.as_view(), name='packages'),
    
    
    url(r'^account/register', api.BankAccountHandler.as_view(), name='mobile-register'),
    url(r'^accounts/', api.MyAccountHandler.as_view(), name='mobile-accounts'),
    url(r'^feeds/', api.FeedsApiHandler.as_view(), name='mobile-feeds'),
    

    url(r'^unionbank/branches/', api.UnionBankBranches.as_view(), name='bank-branches'),
    url(r'^unionbank/accounts/', api.UnionBankAccounts.as_view(), name='bank-accounts'),
    
]