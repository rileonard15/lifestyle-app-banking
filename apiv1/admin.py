from django.contrib import admin
from apiv1.models import Token, BankAccounts, MyAccount
from front.models import Merchant, Company

# Register your models here.
admin.site.register(MyAccount)
admin.site.register(Merchant)
admin.site.register(Company)
