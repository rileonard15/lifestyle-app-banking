from __future__ import unicode_literals

from rest_framework import serializers
from django.db import models
from front.models import Customer, Merchant, Company, Profile, Package
from django.contrib.auth.models import User
import datetime


class Serializer(serializers.ModelSerializer):
    class Meta:
        model = Customer
        fields = ('id','profile','status','owner')


class ProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = ('id','first_name','last_name','middle_name','address','contact_no','province','country','email_add')


class displaySerializer(serializers.ModelSerializer):
    profile = ProfileSerializer()
    class Meta:
        model = Customer
        fields = ('id', 'profile', 'status')




class CustomerHandler():
    def __init__(self, req):
        self.request = req

    def create_customer(self):
        print self.request.user.id
        register = User.objects.create_user(first_name=self.request.data['first_name'],last_name=self.request.data['last_name'],username=self.request.data["email"],email=self.request.data["email"],password=self.request.data["password"])
    	profile = Profile(user=self.request.user,first_name=self.request.data['first_name'],last_name=self.request.data['last_name'],email_add=self.request.data["email"]) 
        profile.save()
        
        self.request.data['profile'] = profile.id

        self.request.data['owner'] = self.request.user.id
        print self.request.data
        cs = Serializer(data=self.request.data)
        if cs.is_valid():
            register.save()

            cs.save()
            return 201, "Created!"
        return 400, cs.errors



    def get_customers(self):
    	customers = Customer.objects.filter(owner=self.request.user.id)
        cs = displaySerializer(data=customers, many=True)
        cs.is_valid()
        return 200, cs.data

