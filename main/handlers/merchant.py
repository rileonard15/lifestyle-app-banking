from __future__ import unicode_literals

from rest_framework import serializers
from django.db import models
from front.models import Merchant


class Serializer(serializers.ModelSerializer):
    def Meta(self):
        models = Merchant
        fields = ('id','email', 'account_status', 'user', 'bank')


class MerchantHandler():
    def __init__(self, req):
        self.request = req