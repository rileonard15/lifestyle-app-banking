from __future__ import unicode_literals

from rest_framework import serializers
from django.db import models
from front.models import Promotion, Branches

from main import helpers as h


class Serializer(serializers.ModelSerializer):
    class Meta:
        model = Promotion
        fields = ('id','title','description','branch','date_to','date_from')

class BranchSerializer(serializers.ModelSerializer):
    class Meta:
        model = Branches
        fields = ('id','name','address','contact_no','manager')

class displaySerializer(serializers.ModelSerializer):
    branch = BranchSerializer()
    date_to = serializers.SerializerMethodField('format_start_date')
    date_from = serializers.SerializerMethodField('format_end_date')

    def format_start_date(self, obj):
        return obj.date_to.strftime("%B %d, %Y")

    def format_end_date(self, obj):
        return obj.date_from.strftime("%B %d, %Y")

    class Meta:
        model = Promotion
        fields = ('id','title','description','branch','date_to','date_from')

    

class PromotionHandler():
    def __init__(self, req):
        self.request = req

    def create_promo(self):
        print self.request.data
        brnch = Branches.objects.get(id=1)
        print 'branchessss...'
        print brnch.name
        promo = Promotion()
        promo.title = self.request.data["title"]
        promo.description = self.request.data['description']
        promo.date_to = h.format_date24(self.request.data["date_to"])
        promo.date_from = h.format_date24(self.request.data["date_from"])
        promo.owner = self.request.user
        promo.save()

        
        promo.branch.add(brnch)

        
        return 201, 'Successfully created!.'

    def get_promo(self):
        promotion = Promotion.objects.filter(owner=self.request.user.id)
        cs = displaySerializer(data=promotion, many=True)
        cs.is_valid()
        return 200, cs.data
