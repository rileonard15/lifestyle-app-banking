from __future__ import unicode_literals

from rest_framework import serializers
from django.db import models
from front.models import Company, Branches


class Serializer(serializers.ModelSerializer):
    class Meta:
        model = Branches
        fields = ('id','name', 'manager', 'address','contact_no','company')
    

class displaySerializer(serializers.ModelSerializer):
    class Meta:
        model = Branches
        fields = ('id','name', 'manager', 'address','contact_no')


class BranchesHandler():
    def __init__(self, req):
        self.request = req

    def create_branch(self):
        print self.request.data
        # company = Company.objects.get(user=self.request.user.id)
        self.request.data['company'] = self.request.user.company.id
        self.request.data["contact_no"] = self.request.data["tel"]

        cs = Serializer(data=[self.request.data], many=True)
        cs.is_valid()
        cs.save()
        return 201, cs.data[0]

    def get_branches(self):
    	branches = Branches.objects.filter(company=self.request.user.company.id)
        cs = displaySerializer(data=branches, many=True)
        cs.is_valid()
        return 200, cs.data