from __future__ import unicode_literals

from rest_framework import serializers
from django.db import models
from front.models import ProfitSharing, Company


class Serializer(serializers.ModelSerializer):
    class Meta:
        model = ProfitSharing
        fields = ('id','amount1','amount2','owner')
    

class ProfitSharingHandler():
    def __init__(self, req):
        self.request = req

    def create_profitsharing(self):
        print self.request.data
        self.request.data['owner'] = self.request.user.id
        
        cs = Serializer(data=self.request.data)
        if cs.is_valid():
            cs.save()
            return 201, "Created!"
        return 400, cs.errors


    def get_profitsharing(self):
        profitsh = ProfitSharing.objects.filter().order_by('-created')
        cs = Serializer(data=profitsh, many=True)
        cs.is_valid()
        return 200, cs.data[0]
