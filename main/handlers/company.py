from __future__ import unicode_literals

from rest_framework import serializers
from django.db import models
from front.models import Company, Merchant
from django.contrib.auth.models import User


class Serializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = ('id', 'industry', 'logo', 'name', 'address', 'contact_no', 'authorized', 'owner')


class CompanyHandler():
    def __init__(self, req):
        self.request = req

    def create_company(self):
        print self.request.data
        print self.request.user.id
    	self.request.data['owner'] = self.request.user.id
        
        cs = Serializer(data=self.request.data)
        if cs.is_valid():
            cs.save()
            return 201, "Created!"
        return 400, cs.errors

    def get_companies(self):
    	companies = Company.objects.filter(owner=self.request.user.id)
        cs = Serializer(data=companies, many=True)
        cs.is_valid()
        return 200, cs.data