from __future__ import unicode_literals

import ast
import datetime

from rest_framework import serializers
from django.db import models
from front.models import Product, Package, Branches
from main.handlers.branches import Serializer as BranchSerializer
from django.contrib.auth.models import User

from main import helpers as h


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ('created', 'points', 'name', 'branch', 'price', 'description', 'merchant',)

class GetProductSerializer(serializers.ModelSerializer):
    branch = BranchSerializer()
    class Meta:
        model = Product
        fields = ('created', 'points', 'name', 'branch', 'price', 'description', 'id',)


class PackageSerializer(serializers.ModelSerializer):
    products = GetProductSerializer(read_only=True, many=True)
    branch = BranchSerializer()
    start_date = serializers.SerializerMethodField('format_start_date')
    exp_date = serializers.SerializerMethodField('format_exp_date')

    def format_start_date(self, obj):
        return obj.start_date.strftime("%m/%d/%Y")

    def format_exp_date(self, obj):
        return obj.start_date.strftime("%m/%d/%Y")

    class Meta:
        model = Package
        fields = ('created', 'name', 'price', 'products', 'description', 'merchant', 'branch', 'start_date', 'exp_date')
        


class SubscriptionHandler():
    def __init__(self, req):
        self.request = req

    def add_product(self):
        self.request.data["merchant"] = self.request.user.id
        serializer = ProductSerializer(data=self.request.data)

        serializer.is_valid()
        serializer.save()

        return 200, serializer.data

    def add_packages(self):
        print self.request.data

        package = Package()
        package.name = self.request.data["name"]
        package.merchant = User.objects.get(id=self.request.user.id)
        package.description = self.request.data["description"]
        package.branch = Branches.objects.get(id=int(self.request.data["branch"]))
        package.start_date = h.format_date24(self.request.data["start_date"])
        package.exp_date = h.format_date24(self.request.data["expire_date"])
        package.price = self.request.data["price"]
        package.save()

        print self.request.data["products"]


        for pid in self.request.data["products"]:
            product = Product.objects.get(id=pid)
            package.products.add(product)


        serializer = PackageSerializer(data=[package], many=True)
        serializer.is_valid()

        return 200, serializer.data

    def get_packages(self):
        packages = Package.objects.filter(merchant=self.request.user.id)

        serializer = PackageSerializer(data=packages, many=True)
        serializer.is_valid()

        return 200, serializer.data

    def get_products(self):
        products = Product.objects.filter(merchant=self.request.user.id)
        serializer = GetProductSerializer(data=products, many=True)
        serializer.is_valid()

        return 200, serializer.data







