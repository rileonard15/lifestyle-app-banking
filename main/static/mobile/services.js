(function() {

    angular
        .module('merchant.service', [])
        .factory('merchantService', merchantService)

    merchantService.$inject = ['$http'];
    function merchantService($http) {
        return {
            getCompany: getCompany,
            createCompany: createCompany,

            getCustomer: getCustomer,
            createCustomer: createCustomer,

            getBranch: getBranch,
            createBranch: createBranch,

            getPackages: getPackages,
            createPackage: createPackage,

            getProducts: getProducts,
            createProduct: createProduct,

            getMerchants: getMerchants,
            getMerchantPackages: getMerchantPackages,
        };

        function getCompany() {
            return $http.get('/web/company').then(function(resp) {
                console.log(resp);
            });
        }

        function createCompany(data) {
            return $http.post('/web/company/', data).then(function(resp) {
                console.log(resp);
            });
        }

        function getCustomer() {
            return $http.get('/web/customer/').then(function(resp) {
                console.log(resp);
            });
        }

        function createCustomer(data) {
            return $http.post('/web/customer/', data).then(function(resp) {
                console.log(resp);
            });
        }

        function getBranch() {
            return $http.get('/web/branches/').then(function(resp) {
                console.log(resp);
                return resp.data;
            });
        }

        function createBranch(data) {
            return $http.post('/web/branches/', data).then(function(resp) {
                console.log(resp);
                return resp.data;
            });
        }

        function getPackages() {
            return $http.get('/web/packages/').then(function(resp) {
                console.log(resp);
                return resp.data
            });
        }

        function getMerchantPackages(data) {
            console.log(data);
            return $http.post('/mobile/apiv1/packages/', data).then(function(resp) {
                console.log(resp);
                return resp.data
            });
        }


        function createPackage(data) {
            return $http.post('/web/packages/', data).then(function(resp) {
                console.log(resp);
            });
        }

        function getProducts() {
            return $http.get('/web/products').then(function(resp) {
                console.log(resp);
                return resp.data;
            });
        }
        function getProfitSharing() {
            return $http.get('/web/profit/sharing/').then(function(resp) {
                console.log(resp);
                return resp.data;
            });
        }
        function createProfitSharing() {
            return $http.post('/web/profit/sharing/', data).then(function(resp) {
                console.log(resp);
                return resp.data;
            });
        }
        function createProduct(data) {
            return $http.post('/web/products/', data).then(function(resp) {
                console.log(resp);
                return resp.data;
            });
        }

        function getMerchants() {
            return $http.get('/mobile/apiv1/merchants/').then(function(resp) {
                console.log(resp);
                return resp.data;
            });
        }
        
    }

})();
