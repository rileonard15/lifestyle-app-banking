(function(){
  'use strict';

  angular
    .module("menu.directive", [])
    .directive("initMenu", initMenu);

  function initMenu(){
    return{
      restrict: 'EA',
      templateUrl: '/static/mobile/modules/menu/views/menu.html',
      controller: 'menuCtrl',
      controllerAs: 'vm',
			link: function(scope, elem, attrs) {
        var page_body = angular.element("body");
				var menu_height = angular.element(".menu").outerHeight();
				page_body.css("padding-bottom", menu_height);
			}
    }
  }
})();
