(function(){
  'use strict';

  angular
    .module("menu.controller", [])
    .controller("menuCtrl", menuCtrl);

  menuCtrl.$inject = ["$scope"];
  function menuCtrl(){
    console.log("menu");
  }
})();
