(function(){
  "use strict";

  angular
    .module("summary.controller", [])
    .controller("summaryCtrl", summaryCtrl);

  summaryCtrl.$inject = ["$scope"];
  function summaryCtrl($scope){
    console.log("summary controller");
    $scope.mobilecontent = '/static/mobile/modules/summary/views/subscriptions.html';

    $scope.opencategories = function(){
      var page_body = angular.element("body");
      page_body.addClass("categories_open");
      $scope.viewcategories = true;
    }
    $scope.closecategories = function(){
      var page_body = angular.element("body");
      page_body.removeClass("categories_open");
      $scope.viewcategories = false;
    }
    $scope.repeat = function(n){
      return new Array(n);
    };
  }
})();
