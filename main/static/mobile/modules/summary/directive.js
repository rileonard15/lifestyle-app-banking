(function(){
  'use strict';

  angular
    .module("summary.directive", [])
    .directive("initSummary", initSummary);

  function initSummary(){
    return{
      restrict: 'EA',
      templateUrl: '/static/mobile/modules/summary/views/summary.html',
      controller: 'summaryCtrl',
      controllerAs: 'vm',
			link: function(scope, elem, attrs) {
        var page_body = angular.element("body");
				var summary_height = angular.element(".summary").outerHeight();
				page_body.css("padding-top", summary_height);
			}
    }
  }
})();
