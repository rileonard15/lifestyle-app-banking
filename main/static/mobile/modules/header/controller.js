(function(){
  'use strict';

  angular
    .module("header.controller", [])
    .controller("headerCtrl", headerCtrl);

  headerCtrl.$inject = ["$scope"];
  function headerCtrl($scope){
    console.log("header");

    $scope.opencategories = function(){
      var page_body = angular.element("body");
      page_body.addClass("categories_open");
      $scope.viewcategories = true;
    }
    $scope.closecategories = function(){
      var page_body = angular.element("body");
      page_body.removeClass("categories_open");
      $scope.viewcategories = false;
    }
  }
})();
