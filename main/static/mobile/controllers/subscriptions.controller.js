(function(){
  "use strict";

  angular
    .module("subscriptions.controller", [])
    .controller("subscriptionsCtrl", subscriptionsCtrl);

  subscriptionsCtrl.$inject = ["$scope"];
  function subscriptionsCtrl($scope){
    console.log("subscriptions controller");

    $scope.activemenu = "subscriptions";
    $scope.mobilecontent = '/static/mobile/views/subscriptions.html';

    $scope.opensuboptions = function(){
      var page_body = angular.element("body");
      page_body.addClass("subscriptoption_open");
      $scope.subscriptionoptions = true;
    }
    $scope.closesuboptions = function(){
      var page_body = angular.element("body");
      page_body.removeClass("subscriptoption_open");
      $scope.subscriptionoptions = false;
    }
    $scope.repeat = function(n){
      return new Array(n);
    };
  }
})();
