(function(){
  "use strict";

  angular
    .module("bills.controller", [])
    .controller("billsCtrl", billsCtrl);

  billsCtrl.$inject = ["$scope", "$timeout"];
  function billsCtrl($scope, $timeout){
    console.log("bills controller");

    $scope.activemenu = "bills";
    $scope.mobilecontent = '/static/mobile/views/bills.html';
    $scope.repeat = function(n){
      return new Array(n);
    };
  }
})();
