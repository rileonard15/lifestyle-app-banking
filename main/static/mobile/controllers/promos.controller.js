(function(){
  "use strict";

  angular
    .module("promos.controller", [])
    .controller("promosCtrl", promosCtrl);

  promosCtrl.$inject = ["$scope", "$timeout"];
  function promosCtrl($scope, $timeout){
    console.log("promos controller");

    $scope.activemenu = "promos";
    $scope.mobilecontent = '/static/mobile/views/promos.html';
    $scope.repeat = function(n){
      return new Array(n);
    };
  }
})();
