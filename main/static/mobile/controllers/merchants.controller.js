(function(){
  "use strict";

  angular
    .module("merchants.controller", [])
    .controller("merchantsCtrl", merchantsCtrl);

  merchantsCtrl.$inject = ["$scope", "$timeout", "merchantService"];
  function merchantsCtrl($scope, $timeout, merchantService){
    console.log("merchants controller");

    $scope.activemenu = "merchants";
    $scope.mobilecontent = '/static/mobile/views/merchants.html';

    $scope.openmerchdetails = function(uid){
      var data = {
        "uid": uid
      }
      merchantService.getMerchantPackages(data).then(function(resp){
        console.log(resp)
      })
      var page_body = angular.element("body");
      page_body.addClass("merchdetails_open");
      $scope.merchdetails = true;
    };
    $scope.closemerchdetails = function(){
      var endAnimation = "webkitAnimationEnd oanimationend msAnimationEnd animationend";
      var page_body = angular.element("body");
      var merchant_details = angular.element(".merchant-details");
      merchant_details.removeClass("slideInLeft").addClass("slideOutLeft").bind(endAnimation, function(){
        merchant_details.unbind(endAnimation);
        $timeout(function(){
          $scope.merchdetails = false;
          page_body.removeClass("merchdetails_open");
        });
      });
    };

    merchants();
    function merchants(){
      merchantService.getMerchants().then(function(resp){
        console.log(resp);
        $scope.companies = resp;
      });
    }

    $scope.availPackage = function(){
      console.log("avail package");
    }

    $scope.repeat = function(n){
      return new Array(n);
    };
  }
})();
