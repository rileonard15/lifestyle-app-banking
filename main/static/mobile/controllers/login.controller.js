(function(){
  "use strict";

  angular
    .module("login.controller", [])
    .controller("loginCtrl", loginCtrl);

  loginCtrl.$inject = ["$scope"];
  function loginCtrl($scope){
    console.log("login controller");
    $scope.currentpage = 'login';

    console.log($scope.currentpage);
  }
})();
