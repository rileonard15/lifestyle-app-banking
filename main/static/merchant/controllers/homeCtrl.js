(function(){
  'use strict';

  angular
    .module("home.controller", [])
    .controller("homeCtrl", homeCtrl);

  homeCtrl.$inject = ["$scope"];
  function homeCtrl($scope){
    console.log("home controller loaded");

    $scope.thecontent = "/static/merchant/views/home.html";
    $scope.leftmenu = "dashboard";

    $scope.chartConfig = {
        options: {
            chart: {
                type: 'spline'
            }
        },
        series: [{
              name: 'Total Customers',
              data: addSeries()
          }, {
              name: 'Active Customers',
              data: addSeries()
          },{
              name: 'Monthly Sales',
              data: addSeries()
          }, {
              name: 'Monthly Payable',
              data: addSeries()
          }],
        title: {
            text: 'Statistics'
        },

         xAxis: {
              categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
          },
        loading: false
      }

    $scope.showCustomers = function() {
      $scope.chartConfig.series = [{ 
        name: 'Total Customers',
        data: addSeries()
      }];
    }

    $scope.showSales = function() {
      $scope.chartConfig.series = [{ 
        name: 'Sales',
        data: addSeries()
      }];
    }

    $scope.showReports = function() {
      $scope.chartConfig.series = [ {
              name: 'Total Customers',
            data: addSeries()
        }, {
            name: 'Active Customers',
            data: addSeries()
        },{
            name: 'Monthly Sales',
            data: addSeries()
        }, {
            name: 'Monthly Payable',
            data: addSeries()
        }];
    }

    function addSeries() {
        var rnd = []
        for (var i = 0; i < 10; i++) {
            rnd.push(Math.floor(Math.random() * 20) + 1)
        }
        return rnd;
    } 
    }
})();
