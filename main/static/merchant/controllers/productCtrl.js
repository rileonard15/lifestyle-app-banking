(function(){
  'use strict';

  angular
    .module("product.controller", [])
    .controller("productCtrl", productCtrl);

    productCtrl.$inject = ['$scope', 'merchantService']
    function productCtrl($scope, merchantService){
        console.log("product controller loaded");

        $scope.thecontent = "/static/merchant/views/product.html";
        $scope.leftmenu = "product";

        getProduct();
        function getProduct(){
          merchantService.getProducts().then(function(resp){
            console.log(resp);
            $scope.products = resp;
          })
        }

        activate();
        function activate() {
          merchantService.getBranch().then(function(resp) {
            console.log(resp);
            $scope.branches = resp;
          });
        }

        $scope.repeat = function(n) {
          return new Array(n);
        }
    }
})();
