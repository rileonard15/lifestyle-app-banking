(function(){
  'use strict';

  angular
    .module("dashboard.controller", [])
    .controller("dashboardCtrl", dashboardCtrl);

    dashboardCtrl.$inject = ['$scope']
    function dashboardCtrl($scope){
      console.log("dashboard controller loaded");
    }
})();
