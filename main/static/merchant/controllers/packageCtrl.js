(function(){
  'use strict';

  angular
    .module("package.controller", [])
    .controller("packageCtrl", packageCtrl);

    packageCtrl.$inject = ['$scope', 'merchantService']
    function packageCtrl($scope, merchantService){
        console.log("package controller loaded");

        $scope.thecontent = "/static/merchant/views/packages.html";
        $scope.leftmenu = "packages";

        $scope.repeat = function(n) {
          return new Array(n);
        }

        activate();
        function activate() {
          merchantService.getBranch().then(function(resp) {
            console.log(resp);
            $scope.branches = resp;
          });
        }

        getPackages();
        function getPackages(){
          merchantService.getPackages().then(function(resp) {
            console.log(resp);
            $scope.packages = resp;
          }); 
        }

        // getProduct();
        // function getProduct(){
        //   merchantService.getProducts().then(function(resp){
        //     console.log(resp);
        //     $scope.products = resp;
        //   })
        // }
    }
})();
