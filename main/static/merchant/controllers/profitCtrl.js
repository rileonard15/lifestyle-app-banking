(function(){
  'use strict';

  angular
    .module("profit.controller", [])
    .controller("profitCtrl", profitCtrl);

    profitCtrl.$inject = ['$scope', 'merchantService']
    function profitCtrl($scope, merchantService){
        console.log("profit controller loaded");

        $scope.thecontent = "/static/merchant/views/profit.html";
        $scope.leftmenu = "profit";
        $scope.profit = {};

        $scope.repeat = function(n) {
          return new Array(n);
        }

        activate();
        function activate() {
          merchantService.getProfitSharing().then(function(resp) {
            $scope.profit = resp;
          });

         
        }

        $scope.saveProfitSharing = function() {
          console.log($scope.profit)
          merchantService.createProfitSharing($scope.profit).then(function(resp) {
            console.log(resp);
          });
        }
    }
})();
