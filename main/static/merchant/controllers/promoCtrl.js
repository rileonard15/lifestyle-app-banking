(function(){
  'use strict';

  angular
    .module("promo.controller", [])
    .controller("promoCtrl", promoCtrl);

    promoCtrl.$inject = ['$scope', 'merchantService']
    function promoCtrl($scope, merchantService){
        console.log("promotions controller loaded");

        $scope.thecontent = "/static/merchant/views/promo.html";
        $scope.leftmenu = "promotions";


        activate();
        function activate() {
          merchantService.getPromo().then(function(resp) {
            console.log('add')
            console.log(resp);
            $scope.promos = resp;
          });

          merchantService.getBranch().then(function(resp) {
            console.log(resp);
            $scope.branches = resp;
          });
        }


        $scope.repeat = function(n) {
          return new Array(n);
        }
    }
})();
