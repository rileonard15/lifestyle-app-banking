(function(){
  'use strict';

  angular
    .module("customers.controller", [])
    .controller("customersCtrl", customersCtrl);

  customersCtrl.$inject = ["$scope", 'merchantService'];
  function customersCtrl($scope, merchantService){
    console.log("customer controller loaded");

    $scope.thecontent = "/static/merchant/views/customers.html";
    $scope.leftmenu = "customers";

    $scope.repeat = function(n) {
      return new Array(n);
    }

    activate();
    function activate() {
      merchantService.getCustomer().then(function(resp) {
        console.log('add')
        console.log(resp);
        $scope.customers = resp;
      });
    }

    $scope.query = [
      { "text": "Tag1" },
      { "text": "Tag2" },
      { "text": "Tag3" },
      { "text": "Tag4" },
      { "text": "Tag5" },
      { "text": "Tag6" },
      { "text": "Tag7" },
      { "text": "Tag8" },
      { "text": "Tag9" },
      { "text": "Tag10" }
    ]

    $scope.loadTags = function() {
      return $scope.query;
    };

    $scope.customerPackage = function(tags) {
      console.log(tags);
    }
  }
})();
