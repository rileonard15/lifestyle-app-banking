(function(){
  'use strict';

  angular
    .module("branches.controller", [])
    .controller("branchesCtrl", branchesCtrl);

  branchesCtrl.$inject = ["$scope", 'merchantService'];
  function branchesCtrl($scope, merchantService){
    console.log("Branches controller loaded");

    $scope.thecontent = "/static/merchant/views/branches.html";
    $scope.leftmenu = "branches";
    $scope.branch = {};

    activate();
    function activate() {
      merchantService.getBranch().then(function(resp) {
        console.log(resp);
        $scope.branches = resp;
      });
    }



  }
})();
