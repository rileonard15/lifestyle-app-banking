(function(){
  'use strict';

  angular
    .module("profile.controller", [])
    .controller("profileCtrl", profileCtrl);

    profileCtrl.$inject = ['$scope']
    function profileCtrl($scope){
        console.log("profileCtrl controller loaded");

        $scope.thecontent = "/static/merchant/views/profile.html";

        $scope.repeat = function(n) {
          return new Array(n);
        }
    }
})();
