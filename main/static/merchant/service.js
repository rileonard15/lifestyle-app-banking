(function() {

    angular
        .module('merchant.service', [])
        .factory('merchantService', merchantService)

    merchantService.$inject = ['$http'];
    function merchantService($http) {
        return {
            createAccount: createAccount,

            getCompany: getCompany,
            createCompany: createCompany,

            getCustomer: getCustomer,
            createCustomer: createCustomer,

            getBranch: getBranch,
            createBranch: createBranch,

            getPackages: getPackages,
            createPackage: createPackage,

            getProducts: getProducts,
            createProduct: createProduct,

            getPromo:  getPromo,
            createPromo: createPromo,

            getProfitSharing: getProfitSharing,
            createProfitSharing: createProfitSharing
        };

        function createAccount(data) {
            return $http.post('/web/merchant/', data).then(function(resp) {
                console.log(resp);
            });
        }

        function getCompany() {
            return $http.get('/web/company').then(function(resp) {
                console.log(resp);
            });
        }

        function createCompany(data) {
            return $http.post('/web/company/', data).then(function(resp) {
                console.log(resp);
            });
        }

        function getCustomer() {
            return $http.get('/web/customer/').then(function(resp) {
                console.log(resp);
                return resp.data;
            });
        }

        function createCustomer(data) {
            return $http.post('/web/customer/', data).then(function(resp) {
                console.log(resp);

            });
        }

        function getBranch() {
            return $http.get('/web/branches/').then(function(resp) {
                console.log(resp);
                return resp.data;
            });
        }

        function createBranch(data) {
            return $http.post('/web/branches/', data).then(function(resp) {
                console.log(resp);
                return resp.data;
            });
        }

        function getPackages() {
            return $http.get('/web/packages/').then(function(resp) {
                console.log(resp);
                return resp.data
            });
        }

        function createPackage(data) {
            return $http.post('/web/packages/', data).then(function(resp) {
                console.log(resp);
            });
        }

        function getProducts() {
            return $http.get('/web/products').then(function(resp) {
                console.log(resp);
                return resp.data;
            });
        }
        function getProfitSharing() {
            return $http.get('/web/profit/sharing/').then(function(resp) {
                console.log(resp);
                return resp.data;
            });
        }
        function createProfitSharing(data) {
            return $http.post('/web/profit/sharing/', data).then(function(resp) {
                console.log(resp);
                return resp.data;
            });
        }
        function createProduct(data) {
            return $http.post('/web/products/', data).then(function(resp) {
                console.log(resp);
                return resp.data;
            });
        }

        function createPromo(data) {
            console.log(data)
            return $http.post('/web/feeds/promo/', data).then(function(resp) {
                console.log(resp);
                return resp.data;
            });
        }
        function getPromo() {
            return $http.get('/web/feeds/promo/').then(function(resp) {
                console.log(resp);
                return resp.data
            });
        }
        
    }

})();
