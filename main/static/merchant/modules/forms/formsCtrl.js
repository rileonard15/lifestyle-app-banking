(function(){
  'use strict';

  angular
    .module("forms.controller", [])
    .controller("formsCtrl", formsCtrl);

  formsCtrl.$inject = ["$scope", "merchantService"];
  function formsCtrl($scope, merchantService){
    console.log("forms controller loaded");

    $scope.company = {};
    $scope.customer = {};
    $scope.promo = {};
    $scope.profit = {};
    $scope.branch = {};
    $scope.product = {};
    $scope.package = {};
    $scope.account = {};

    $scope.query = [
      { "text": "Tag1" },
      { "text": "Tag2" },
      { "text": "Tag3" },
      { "text": "Tag4" },
      { "text": "Tag5" },
      { "text": "Tag6" },
      { "text": "Tag7" },
      { "text": "Tag8" },
      { "text": "Tag9" },
      { "text": "Tag10" }
    ]

    $scope.theProducts = {}
    $scope.loadTags = function() {
      return merchantService.getProducts().then(function(resp){
          console.log(resp);
          var data = [];
          for(var i=0; i<resp.length; i++){
            data.push({"text": resp[i].name})
            $scope.theProducts[resp[i].name] = resp[i].id
          }
          return data;
      })
    };

    $scope.openform = function(){
      $scope.pageform = true;
    }
    $scope.closeform = function(){
      $scope.pageform = false;
    }
    $scope.savePromo = function() {
      console.log($scope.promo)
      merchantService.createPromo($scope.promo).then(function(resp) {
        console.log(resp);
      });
    }
    
    
    $scope.saveCompany = function() {
      merchantService.createCompany($scope.company).then(function(resp) {
        console.log(resp);
      });
    }
    $scope.saveCustomer = function() {
      merchantService.createCustomer($scope.customer).then(function(resp) {
        console.log(resp);
      });
    }
    $scope.saveBranch = function() {
      merchantService.createBranch($scope.branch).then(function(resp) {
        merchantService.getBranch().then(function(resp) {
          $scope.branch = {};
          $scope.branches = resp;
        });
      });
    }

    $scope.savePackages = function() {
      merchantService.createPackage($scope.packages).then(function(resp) {
        console.log(resp);
      });
    }

    $scope.product = {};
    $scope.saveProduct = function(){
      merchantService.createProduct($scope.product).then(function(resp){
        console.log(resp);
        merchantService.getProducts().then(function(resp){
          $scope.product = {};
          $scope.products = resp;
        })
      })
    }

  $scope.package = {};
  $scope.savePackage = function(){
    console.log($scope.package)
    var data = {};
    data["description"] = $scope.package.description;
    data["branch"] = $scope.package.branch;
    data["expire_date"] = $scope.package.expire_date;
    data["start_date"] = $scope.package.start_date;
    data["name"] = $scope.package.name;
    data["price"] = $scope.package.price;
    data["products"] = []

    for(var i=0; i<$scope.package.products.length; i++){
      data["products"].push($scope.theProducts[$scope.package.products[i].text]);
    }

    merchantService.createPackage(data).then(function(resp){
      merchantService.getPackages().then(function(resp) {
        $scope.packages = resp;
      }); 
    });
  }

  }
})();
