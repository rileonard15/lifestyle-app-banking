(function(){
  'use strict';

  angular
    .module("forms.directives", [])
    .directive("initCustomerform", initCustomerform)
    .directive("initCompanyform", initCompanyform)
    .directive("initBranchform", initBranchform)
    .directive("initProductform", initProductform)
    .directive("initPackageform", initPackageform)
    .directive("initPromoform", initPromoform)
    .directive('datepicker', datepicker)

    function initCustomerform(){
      return {
  			restrict: 'EA',
        templateUrl: '/static/merchant/modules/forms/views/customer_form.html',
        controller: 'formsCtrl',
        controllerAs: 'vm'
  		}
    }
    function initCompanyform(){
      return {
  			restrict: 'EA',
        templateUrl: '/static/merchant/modules/forms/views/company_form.html',
        controller: 'formsCtrl',
        controllerAs: 'vm'
  		}
    }
    function initBranchform(){
      return {
  			restrict: 'EA',
        templateUrl: '/static/merchant/modules/forms/views/branches_form.html',
        controller: 'formsCtrl',
        controllerAs: 'vm'
  		}
    }

    function initProductform() {
      return {
        restrict: 'EA',
        templateUrl: '/static/merchant/modules/forms/views/product_form.html',
        controller: 'formsCtrl',
        controllerAs: 'vm'
      }
    }

    function initPromoform() {
      return {
        restrict: 'EA',
        templateUrl: '/static/merchant/modules/forms/views/promo_form.html',
        controller: 'formsCtrl',
        controllerAs: 'vm'
      }
    }

    function datepicker() {
      return {
        restrict: 'EA',
        link: function(scope, elem, attrs) {
          elem.datepicker();
        }
      }
    }

    function initPackageform() {
      return {
        restrict: 'EA',
        templateUrl: '/static/merchant/modules/forms/views/package_form.html',
        controller: 'formsCtrl',
        controllerAs: 'vm'
      }
    }
})();
