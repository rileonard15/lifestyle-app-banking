(function() {

	angular
		.module('header.directives', [])
		.directive('initHeadermerchant', initHeadermerchant);

	function initHeadermerchant() {
		return {
			restrict: 'EA',
      templateUrl: '/static/merchant/modules/header/views/header.html',
      controller: 'headerCtrl',
      controllerAs: 'vm',
			link: function(scope, elem, attrs) {
        var page_body = angular.element("body");
        var header_height = angular.element("header").outerHeight();
        console.log(header_height);
        page_body.css("padding-top", header_height);
			}
		}
	}
})();
