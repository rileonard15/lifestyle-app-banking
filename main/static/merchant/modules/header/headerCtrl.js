(function(){
  'use strict';

  angular
    .module("header.controller", [])
    .controller("headerCtrl", headerCtrl);

  headerCtrl.$inject = ["$scope"];
  function headerCtrl($scope){
    console.log("header controller loaded");
  }
})();
