(function() {

	angular
		.module('leftsidebar.directives', [])
		.directive('initLeftsidebar', initLeftsidebar);

	function initLeftsidebar() {
		return {
			restrict: 'EA',
      templateUrl: '/static/merchant/modules/leftsidebar/views/leftsidebar.html',
      controller: 'leftsidebarCtrl',
      controllerAs: 'vm',
			link: function(scope, elem, attrs) {
        var page_body = angular.element("body");
				var left_sidebar = angular.element(".leftsidebar");
				var header_height = angular.element("header").outerHeight();
        var leftsidebar_width = angular.element(".leftsidebar").outerWidth();
				page_body.css("padding-left", leftsidebar_width);
        left_sidebar.css("padding-top", header_height);
			}
		}
	}
})();
