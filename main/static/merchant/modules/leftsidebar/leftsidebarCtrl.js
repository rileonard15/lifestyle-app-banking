(function(){
  'use strict';

  angular
    .module("leftsidebar.controller", [])
    .controller("leftsidebarCtrl", leftsidebarCtrl);

    leftsidebarCtrl.$inject = ["$scope"];
    function leftsidebarCtrl($scope){
      console.log("leftsidebar controller loaded");
    }
})();
