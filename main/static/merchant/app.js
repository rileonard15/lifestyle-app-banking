(function(){
  'use strict';

  angular.module("lifestyle", [
    'ui.router',
    'ngTagsInput',
    'highcharts-ng',
    // controller
    'dashboard.controller',
    'home.controller',
    'customers.controller',
    'branches.controller',
    'product.controller',
    'promo.controller',
    'package.controller',
    'profit.controller',
    'profile.controller',
    // module header
    'header.controller',
    'header.directives',
    // module leftsidebar
    'leftsidebar.controller',
    'leftsidebar.directives',
    // module forms
    'forms.controller',
    'forms.directives',

    'merchant.service'
  ]).config(appConfig);

  appConfig.$inject = ["$stateProvider", "$urlRouterProvider", "$httpProvider"];
  function appConfig($stateProvider, $urlRouterProvider, $httpProvider){
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
    $urlRouterProvider.otherwise('/');

    $stateProvider
      .state("dashboard", {
        url: '/',
        templateUrl: '/static/merchant/views/dashboard.html',
        controller: 'homeCtrl',
        controllerAs: 'vm'
      })
      .state("profile", {
        url: '/profile',
        templateUrl: '/static/merchant/views/dashboard.html',
        controller: 'profileCtrl',
        controllerAs: 'vm'
      })
      .state("customers", {
        url: '/customers',
        templateUrl: '/static/merchant/views/dashboard.html',
        controller: 'customersCtrl',
        controllerAs: 'vm'
      })
      .state("branches", {
        url: '/branches',
        templateUrl: '/static/merchant/views/dashboard.html',
        controller: 'branchesCtrl',
        controllerAs: 'vm'
      })
      .state("products", {
        url: '/products',
        templateUrl: '/static/merchant/views/dashboard.html',
        controller: 'productCtrl',
        controllerAs: 'vm'
      })
      .state("promotions", {
        url: '/promotions',
        templateUrl: '/static/merchant/views/dashboard.html',
        controller: 'promoCtrl',
        controllerAs: 'vm'
      })
      .state("packages", {
        url: '/packages',
        templateUrl: '/static/merchant/views/dashboard.html',
        controller: 'packageCtrl',
        controllerAs: 'vm'
      })
      .state("profit", {
        url: '/profit',
        templateUrl: '/static/merchant/views/dashboard.html',
        controller: 'profitCtrl',
        controllerAs: 'vm'
      })
  }
})();
