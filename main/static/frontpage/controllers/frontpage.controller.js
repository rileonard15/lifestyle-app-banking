(function(){
  'use strict';

  angular
    .module("frontpage.controller", [])
    .controller("frontpageCtrl", frontpageCtrl);

  frontpageCtrl.$inject = ["$scope"];
  function frontpageCtrl(){
    console.log("frontpage controller loaded");
    var winscreen = angular.element(window);

    function mainblock(){
      var winheight = angular.element(window).outerHeight();
      var blockmain = angular.element("#mainblock");

      blockmain.css("min-height", winheight);
      console.log(winheight);
    };
    mainblock();
    
    winscreen.resize(function(){
      mainblock();
    });
  }
})();
