(function(){
  'use strict';

  angular
    .module("header.controller", [])
    .controller("headerCtrl", headerCtrl);

  headerCtrl.$inject = ["$scope", "merchantService"];
  function headerCtrl($scope, merchantService){
    var vm = this;
    var winscreen = angular.element(window);
    console.log("header controller loaded");

    function fixedheader(){
      var winscroll = angular.element(window).scrollTop();
      var pageheader = angular.element("header");
      console.log(winscroll);
      if(winscroll >= 20){
        pageheader.addClass("fixed");
      }else{
        pageheader.removeClass("fixed");
      }
    };

    $scope.saveAccount = function() {
      console.log($scope.account)
      merchantService.createAccount($scope.account).then(function(resp) {
        console.log(resp);
      });
    }

    // login and forgot password
    $scope.openLoginAccount = function(obj){
      var usermenu_bttns = angular.element(".user-menu li a");
      var targetbttn = angular.element(obj.currentTarget);
      usermenu_bttns.removeClass("active");
      targetbttn.addClass("active");
      $scope.ddsignup = false;
      $scope.ddlogin = true;
      $scope.signinaccount = true;
      $scope.forgotpassword = false;
    };
    $scope.cancelLoginAccount = function(){
      var usermenu_bttns = angular.element(".user-menu li a");
      usermenu_bttns.removeClass("active");
      $scope.ddlogin = false;
      $scope.signinaccount = false;
      $scope.forgotpassword = false;
    };
    $scope.showLoginAccount = function(){
      $scope.signinaccount = true;
      $scope.forgotpassword = false;
    };
    $scope.showForgotPassword = function(){
      $scope.signinaccount = false;
      $scope.forgotpassword = true;
    };

    // create account
    $scope.openCreateAccount = function(obj){
      var usermenu_bttns = angular.element(".user-menu li a");
      var targetbttn = angular.element(obj.currentTarget);
      usermenu_bttns.removeClass("active");
      targetbttn.addClass("active");
      $scope.ddsignup = true;
      $scope.ddlogin = false;
    };
    $scope.cancelCreateAccount = function(){
      var usermenu_bttns = angular.element(".user-menu li a");
      usermenu_bttns.removeClass("active");
      $scope.ddsignup = false;
      $scope.ddlogin = false;
    };
    winscreen.scroll(function(){
      fixedheader();
    });
  }
})();
