(function(){
  'use strict';

  angular
    .module("header.directive", [])
    .directive("initHeader", initHeader);

  function initHeader(){
    return {
			restrict: 'EA',
      templateUrl: '/static/frontpage/modules/header/views/header.html',
      controller: 'headerCtrl',
      controllerAs: 'vm',
			link: function(scope, elem, attrs) {
        var page_body = angular.element("#mainblock");
        var header_height = angular.element("header").outerHeight();
        page_body.css("padding-top", header_height + 20);
			}
		}
  }
})();
