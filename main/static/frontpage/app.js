(function(){
  'use strict';

  angular.module("frontpage", [
    'ui.router',
    // page controller
    'frontpage.controller',
    // modules
    'header.controller',
    'header.directive',
    'merchant.service',
  ]).config(appConfig);

  appConfig.$inject = ["$stateProvider", "$urlRouterProvider"];
  function appConfig($stateProvider, $urlRouterProvider){
    $urlRouterProvider.otherwise('/');

    $stateProvider
      .state("dashboard", {
        url: '/',
        templateUrl: '/static/frontpage/views/frontpage.html',
        controller: 'frontpageCtrl',
        controllerAs: 'vm'
      });
  }
})();
