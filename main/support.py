from __future__ import unicode_literals

from rest_framework import serializers
from django.db import models
from main.models import Company, Support


class Serializer(serializers.ModelSerializer):
    def Meta(self):
        models = Support
        fields = ('id','branch','complain')


class CompanyHandler():
    def __init__(self, req):
        self.request = req

    def create_company(self):
    	merchant = Merchant.objects.get(user=self.request.user.id)
    	if not merchant:
    		return 404, 'Not Found.'
    	self.request.data['merchant'] = merchant
    	self.request.data['user'] = user

        cs = Serializer(data=[self.request.data], many=True)
        cs.is_valid()
        return 201, cs.data[0]

    def get_companies(self):
    	companies = Company.objects.filter(owner=self.request.user.id)
        cs = Serializer(data=companies, many=True)
        cs.is_valid()
        return 200, cs.data