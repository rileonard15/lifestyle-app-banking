from django.conf.urls import include, url
from front import views, api

urlpatterns = [

    url(r'^merchant/', views.MerchantPage.as_view(), name='merchant-user'),
    url(r'^company/', api.CompanyPage.as_view(), name='company-info'),
    url(r'^branches/', api.BranchesPage.as_view(), name='company-info'),
    url(r'^customer/', api.CustomerPage.as_view(), name='company-info'),
    url(r'^profit/sharing/', api.ProfitSharingPage.as_view(), name='profit-sharing'),
    url(r'^feeds/promo/', api.PromotionPage.as_view(), name='promotion'),
    

    url(r'^products/', api.ProductApiHandler.as_view(), name='product'),
    url(r'^packages/', api.PackageApiHandler.as_view(), name='packages'),
    
]
