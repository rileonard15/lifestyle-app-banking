import re
import string
import random
import math
import datetime

EMAIL_REGEX = re.compile(r"[^@]+@[^@]+\.[^@]+")


def generate_code(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


def convert_to_money(value):
    return format(math.floor(int(value) * 100) / 100, ',.2f')


def convert_to_percentage(value):
    return format(math.floor(int(value) * 100) / 100, ',.0f')


def convert_to_date(date):
    d = datetime.datetime.strptime(date, "%m/%d/%Y")
    return d.strftime("%B %d, %Y")


def format_single_date(date):

    sdate = date + "T00:00:00"

    start_date = datetime.datetime.strptime(sdate, "%Y-%m-%dT%H:%M:%S") # start date

    # tz = pytz.timezone("Asia/Manila")
    # aware_startDate = tz.localize(start_date, is_dst=None)
    
    return start_date

def format_date24(date):

    sdate = date + "T00:00:00"

    start_date = datetime.datetime.strptime(sdate, "%m/%d/%YT%H:%M:%S") # start date

    # tz = pytz.timezone("Asia/Manila")
    # aware_startDate = tz.localize(start_date, is_dst=None)
    
    return start_date

