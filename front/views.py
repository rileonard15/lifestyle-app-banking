import base64
import urllib2
import urllib
import json

from django.shortcuts import render
from django.contrib.auth import authenticate, login, models
from django.contrib.auth import logout
from django.shortcuts import redirect

from main.base import BaseHandler
from main import helpers as h

from front.models import Merchant
from django.contrib.auth.models import User


class FrontPage(BaseHandler):
    def get(self, request):
        if self.request.user.is_authenticated():
            print request.user.groups.all()[0].name
            if request.user.groups.all()[0].name == "CUSTOMER":
                self.tv["current_page"] = "customer-dashboard"
                return self.render(request, 'mobile.html')
            else:
                self.tv["current_page"] = "dashboard"
                return self.render(request, 'front/dashboard.html')

        self.tv["current_page"] = "frontpage"
        return self.render(request, 'front/index.html')


    def post(self, request):
        email = self.request.data["email"]
        password = self.request.data["password"]

        if h.EMAIL_REGEX.match(email):
            try:
                user = User.objects.get(email=email)
            except User.DoesNotExist:
                request.session["error"] = 'Email does not existbb..'
                request.session.set_expiry(10)
                return redirect("/")
        else:
            try:
                user = User.objects.get(username=email)
            except User.DoesNotExist:
                request.session["error"] = 'Email does not existnnn..'
                request.session.set_expiry(10)
                return redirect("/")

        if user.is_active == False:
            request.session["error"] = 'Email does not exisdddt..'
            request.session.set_expiry(10)
            return redirect("/")


        if user.check_password(password):
            user.backend = 'django.contrib.auth.backends.ModelBackend'
            login(request, user)
            return redirect("/dashboard/")
        else:
            request.session["error"] = 'Email and Password does not match..'
            request.session.set_expiry(10)
            return redirect("/")

class DashboardPage(BaseHandler):
    def get(self, request):
        return redirect("/")


class LogoutHandler(BaseHandler):
    def get(self, request):
        logout(request)
        return redirect("/")


class MerchantPage(BaseHandler):
    def get(self, request):
        pass

    def post(self, request):

        print self.request.data
        try:
            user = User.objects.get(email=self.request.data["email"])
        except User.DoesNotExist:

            register = User.objects.create_user(first_name=self.request.data['fullname'],username=self.request.data["email"],email=self.request.data["email"],password=self.request.data["password"])

            g = models.Group.objects.get(name="MERCHANTS")
            if not g:
                return self.api_response(400, 'Error, Group not found!')

            merchant = Merchant()
            merchant.user = register

            g.user_set.add(register)
            if register and g:
                register.save()
                merchant.save()

            return self.api_response(200, 'Successfully save merchant user!')
        return self.api_response(400, 'Email already exist!.')
