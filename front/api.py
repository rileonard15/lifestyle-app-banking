import base64
import urllib2
import urllib
import json

from django.contrib.auth import authenticate, login
from django.shortcuts import render

from main.base import BaseHandler
from main import helpers as h


from main.handlers.company import CompanyHandler
from main.handlers.subscription import SubscriptionHandler
from main.handlers.customer import CustomerHandler
from main.handlers.branches import BranchesHandler
from main.handlers.profitsharing import ProfitSharingHandler
from main.handlers.promotion import PromotionHandler


class CompanyPage(BaseHandler):
	def get(self, request):
		code, response = CompanyHandler(request).get_companies()
		return self.api_response(code, response)

	def post(self, request):
		print self.request.data
		code, response = CompanyHandler(request).create_company()
		return self.api_response(code, response)

class CustomerPage(BaseHandler):
	def get(self, request):
		code, response = CustomerHandler(request).get_customers()
		print response
		return self.api_response(code, response)

	def post(self, request):
		print self.request.data
		code, response = CustomerHandler(request).create_customer()
		return self.api_response(code, response)

class BranchesPage(BaseHandler):
	def get(self, request):
		code, response = BranchesHandler(request).get_branches()
		return self.api_response(code, response)

	def post(self, request):
		code, response = BranchesHandler(request).create_branch()
		return self.api_response(code, response)

        

class ProductApiHandler(BaseHandler):
	def get(self, request):
		code, response = SubscriptionHandler(request).get_products()
		return self.api_response(code, response)


	def post(self, request):
		code, response = SubscriptionHandler(request).add_product()

		return self.api_response(code, response)


class PackageApiHandler(BaseHandler):
	def get(self, request):
		code, response = SubscriptionHandler(request).get_packages()

		return self.api_response(code, response)
		
	def post(self, request):
		code, response = SubscriptionHandler(request).add_packages()

		return self.api_response(code, response)
		

class ProfitSharingPage(BaseHandler):
	def get(self, request):
		code, response = ProfitSharingHandler(request).get_profitsharing()

		return self.api_response(code, response)
	def post(self, request):
		print self.request.data
		code, response = ProfitSharingHandler(request).create_profitsharing()

		return self.api_response(code, response)

class PromotionPage(BaseHandler):
	def get(self, request):
		code, response = PromotionHandler(request).get_promo()
		return self.api_response(code, response)

	def post(self,request):
		code, response = PromotionHandler(request).create_promo()
		return self.api_response(code, response)


