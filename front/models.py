from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.db import models


class Profile(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    user = models.ForeignKey(User)
    first_name = models.CharField(max_length=50, null=True)
    last_name = models.CharField(max_length=50, null=True)
    middle_name = models.CharField(max_length=50, null=True)
    address = models.CharField(max_length=50, null=True)
    contact_no = models.CharField(max_length=50, null=True)
    province = models.CharField(max_length=50, null=True)
    country = models.CharField(max_length=50, null=True)
    email_add = models.CharField(max_length=50, null=True)


class Merchant(models.Model):
    account_status = models.CharField(max_length=50, null=True)
    user = models.ForeignKey(User)


class Company(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    industry = models.CharField(max_length=50,null=True)
    logo = models.CharField(max_length=100,null=True)
    name = models.CharField(max_length=50,null=True)
    address = models.CharField(max_length=50,null=True)
    latlong = models.CharField(max_length=50,null=True)
    contact_no = models.CharField(max_length=50,null=True)
    authorized = models.CharField(max_length=50,null=True)
    owner = models.OneToOneField(User, null=True)


class Branches(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    name = models.CharField(max_length=50, null=True, blank=True)
    address = models.CharField(max_length=50, null=True, blank=True)
    latlong = models.CharField(max_length=50,null=True)
    contact_no = models.CharField(max_length=50, null=True, blank=True)
    manager = models.CharField(max_length=150, null=True, blank=True)
    company = models.ForeignKey(Company)


class Product(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    name = models.CharField(max_length=200, null=True)
    price = models.FloatField(default=0)
    points = models.FloatField(default=0)
    description = models.TextField(null=True)
    merchant = models.ForeignKey(User, null=True)
    branch = models.ForeignKey(Branches, null=True)


class Package(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    name = models.CharField(max_length=50, null=True)
    description = models.CharField(max_length=50, null=True)
    exp_date = models.DateTimeField(null=True)
    products = models.ManyToManyField(Product)
    price = models.FloatField(default=0)
    merchant = models.ForeignKey(User, null=True)
    start_date = models.DateTimeField(null=True)
    branch = models.ForeignKey(Branches, null=True)


class Services(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    name = models.CharField(max_length=50, null=True)
    description = models.CharField(max_length=50, null=True)
    price = models.IntegerField()
    points = models.IntegerField()
    branch = models.ForeignKey(Branches)


class Customer(models.Model):
    profile = models.ForeignKey(Profile)
    status = models.BooleanField(default=True)
    owner = models.ForeignKey(User)
    assign_package = models.ManyToManyField(Package)
    

class Promotion(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    title = models.CharField(max_length=50, null=True)
    description = models.CharField(max_length=50, null=True)
    branch = models.ManyToManyField(Branches)
    date_to = models.DateTimeField()
    date_from = models.DateTimeField()
    owner = models.ForeignKey(User)


class Support(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    branch = models.ManyToManyField(Branches)
    complain = models.CharField(max_length=50, null=True)


class ProfitSharing(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    amount1 = models.IntegerField()
    amount2 = models.IntegerField()
    owner = models.ForeignKey(User)
    

