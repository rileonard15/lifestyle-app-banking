Django==1.9.6
django-cors-headers==1.1.0
django-filter==0.10.0
djangorestframework==3.3.2
Markdown==2.6.2
MySQL-python==1.2.5
wheel==0.24.0
