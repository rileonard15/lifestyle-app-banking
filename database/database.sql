-- MySQL dump 10.13  Distrib 5.6.25, for osx10.10 (x86_64)
--
-- Host: localhost    Database: ustyle
-- ------------------------------------------------------
-- Server version	5.6.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `apiv1_bankaccounts`
--

DROP TABLE IF EXISTS `apiv1_bankaccounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apiv1_bankaccounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime(6) NOT NULL,
  `updated` datetime(6) NOT NULL,
  `refid` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`),
  CONSTRAINT `apiv1_bankaccounts_user_id_b57f8af0_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apiv1_bankaccounts`
--

LOCK TABLES `apiv1_bankaccounts` WRITE;
/*!40000 ALTER TABLE `apiv1_bankaccounts` DISABLE KEYS */;
/*!40000 ALTER TABLE `apiv1_bankaccounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apiv1_feeds`
--

DROP TABLE IF EXISTS `apiv1_feeds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apiv1_feeds` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime(6) NOT NULL,
  `updated` datetime(6) NOT NULL,
  `content` longtext NOT NULL,
  `tag` varchar(200) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `apiv1_feeds_user_id_2ff7f0f8_fk_auth_user_id` (`user_id`),
  CONSTRAINT `apiv1_feeds_user_id_2ff7f0f8_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apiv1_feeds`
--

LOCK TABLES `apiv1_feeds` WRITE;
/*!40000 ALTER TABLE `apiv1_feeds` DISABLE KEYS */;
/*!40000 ALTER TABLE `apiv1_feeds` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apiv1_myaccount`
--

DROP TABLE IF EXISTS `apiv1_myaccount`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apiv1_myaccount` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime(6) NOT NULL,
  `updated` datetime(6) NOT NULL,
  `balance` double NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`),
  CONSTRAINT `apiv1_myaccount_user_id_af526d40_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apiv1_myaccount`
--

LOCK TABLES `apiv1_myaccount` WRITE;
/*!40000 ALTER TABLE `apiv1_myaccount` DISABLE KEYS */;
/*!40000 ALTER TABLE `apiv1_myaccount` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apiv1_mysubscription`
--

DROP TABLE IF EXISTS `apiv1_mysubscription`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apiv1_mysubscription` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime(6) NOT NULL,
  `updated` datetime(6) NOT NULL,
  `merchant_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `apiv1_mysubscription_merchant_id_67e41579_fk_auth_user_id` (`merchant_id`),
  KEY `apiv1_mysubscription_user_id_beb6a6c2_fk_auth_user_id` (`user_id`),
  CONSTRAINT `apiv1_mysubscription_merchant_id_67e41579_fk_auth_user_id` FOREIGN KEY (`merchant_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `apiv1_mysubscription_user_id_beb6a6c2_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apiv1_mysubscription`
--

LOCK TABLES `apiv1_mysubscription` WRITE;
/*!40000 ALTER TABLE `apiv1_mysubscription` DISABLE KEYS */;
/*!40000 ALTER TABLE `apiv1_mysubscription` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apiv1_token`
--

DROP TABLE IF EXISTS `apiv1_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apiv1_token` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime(6) NOT NULL,
  `updated` datetime(6) NOT NULL,
  `key` varchar(200) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`),
  CONSTRAINT `apiv1_token_user_id_b31f8b25_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apiv1_token`
--

LOCK TABLES `apiv1_token` WRITE;
/*!40000 ALTER TABLE `apiv1_token` DISABLE KEYS */;
/*!40000 ALTER TABLE `apiv1_token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
INSERT INTO `auth_group` VALUES (2,'CUSTOMER'),(1,'MERCHANTS');
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissi_permission_id_84c5c92e_fk_auth_permission_id` (`permission_id`),
  CONSTRAINT `auth_group_permissi_permission_id_84c5c92e_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_01ab375a_uniq` (`content_type_id`,`codename`),
  CONSTRAINT `auth_permissi_content_type_id_2f476e4b_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add log entry',1,'add_logentry'),(2,'Can change log entry',1,'change_logentry'),(3,'Can delete log entry',1,'delete_logentry'),(4,'Can add permission',2,'add_permission'),(5,'Can change permission',2,'change_permission'),(6,'Can delete permission',2,'delete_permission'),(7,'Can add group',3,'add_group'),(8,'Can change group',3,'change_group'),(9,'Can delete group',3,'delete_group'),(10,'Can add user',4,'add_user'),(11,'Can change user',4,'change_user'),(12,'Can delete user',4,'delete_user'),(13,'Can add content type',5,'add_contenttype'),(14,'Can change content type',5,'change_contenttype'),(15,'Can delete content type',5,'delete_contenttype'),(16,'Can add session',6,'add_session'),(17,'Can change session',6,'change_session'),(18,'Can delete session',6,'delete_session'),(19,'Can add token',7,'add_token'),(20,'Can change token',7,'change_token'),(21,'Can delete token',7,'delete_token'),(22,'Can add cors model',8,'add_corsmodel'),(23,'Can change cors model',8,'change_corsmodel'),(24,'Can delete cors model',8,'delete_corsmodel'),(25,'Can add profile',9,'add_profile'),(26,'Can change profile',9,'change_profile'),(27,'Can delete profile',9,'delete_profile'),(28,'Can add merchant',10,'add_merchant'),(29,'Can change merchant',10,'change_merchant'),(30,'Can delete merchant',10,'delete_merchant'),(31,'Can add company',11,'add_company'),(32,'Can change company',11,'change_company'),(33,'Can delete company',11,'delete_company'),(34,'Can add branches',12,'add_branches'),(35,'Can change branches',12,'change_branches'),(36,'Can delete branches',12,'delete_branches'),(37,'Can add product',13,'add_product'),(38,'Can change product',13,'change_product'),(39,'Can delete product',13,'delete_product'),(40,'Can add package',14,'add_package'),(41,'Can change package',14,'change_package'),(42,'Can delete package',14,'delete_package'),(43,'Can add services',15,'add_services'),(44,'Can change services',15,'change_services'),(45,'Can delete services',15,'delete_services'),(46,'Can add customer',16,'add_customer'),(47,'Can change customer',16,'change_customer'),(48,'Can delete customer',16,'delete_customer'),(49,'Can add promotion',17,'add_promotion'),(50,'Can change promotion',17,'change_promotion'),(51,'Can delete promotion',17,'delete_promotion'),(52,'Can add support',18,'add_support'),(53,'Can change support',18,'change_support'),(54,'Can delete support',18,'delete_support'),(55,'Can add profit sharing',19,'add_profitsharing'),(56,'Can change profit sharing',19,'change_profitsharing'),(57,'Can delete profit sharing',19,'delete_profitsharing'),(58,'Can add bank accounts',20,'add_bankaccounts'),(59,'Can change bank accounts',20,'change_bankaccounts'),(60,'Can delete bank accounts',20,'delete_bankaccounts'),(61,'Can add token',21,'add_token'),(62,'Can change token',21,'change_token'),(63,'Can delete token',21,'delete_token'),(64,'Can add my account',22,'add_myaccount'),(65,'Can change my account',22,'change_myaccount'),(66,'Can delete my account',22,'delete_myaccount'),(67,'Can add feeds',23,'add_feeds'),(68,'Can change feeds',23,'change_feeds'),(69,'Can delete feeds',23,'delete_feeds'),(70,'Can add my subscription',24,'add_mysubscription'),(71,'Can change my subscription',24,'change_mysubscription'),(72,'Can delete my subscription',24,'delete_mysubscription');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(30) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
INSERT INTO `auth_user` VALUES (1,'pbkdf2_sha256$24000$fwR1BNIGKlLN$utuEsj2gLadHw+98qbXX22NDapjDGRBoEDh0QHjq12U=','2016-08-12 08:47:40.454510',1,'administrator','','','mojadofaye@gmail.com',1,1,'2016-08-12 08:47:24.398200'),(2,'pbkdf2_sha256$24000$y0TD8pm7UHhd$n57xiHWmWZQ5gCkRQz4lki2jSl2Tljq4PH6ToMcXql0=','2016-08-12 08:49:37.937657',0,'rodel@lagahit.com','Rodel','Lagahit','rodel@lagahit.com',0,1,'2016-08-12 08:48:31.000000'),(5,'pbkdf2_sha256$24000$dCow1ZLwnsFF$U+Y6N8pzAZjrNuRbYwtQS3LXRzlDwpDS9yLdwUcfpgs=',NULL,0,'padopu@yahoo.com','Ferris','Mann','padopu@yahoo.com',0,1,'2016-08-12 09:52:52.743818'),(7,'pbkdf2_sha256$24000$Fde2pTbF6MXI$rK0AMqmE2YRF/4U6UkEXRkNIrLv339+lKInrgV7pC3I=',NULL,0,'teniravivi@gmail.com','Idola','Mathews','teniravivi@gmail.com',0,1,'2016-08-12 09:58:33.136037'),(8,'pbkdf2_sha256$24000$EVVQW5PaS1eT$akfI8U+/fEVn3xUXrPEhXWzKh23tJGp8xfrETVeriVY=',NULL,0,'moqox@hotmail.com','Donna','Watts','moqox@hotmail.com',0,1,'2016-08-12 10:03:06.427363'),(9,'pbkdf2_sha256$24000$PmFYIcHlvSlT$5bf1h4dS+NAebylGgLWuTHfWw0DKt15ozmVsnJrnSgY=',NULL,0,'besowojut@yahoo.com','Tanner','Olsen','besowojut@yahoo.com',0,1,'2016-08-12 10:03:31.901122'),(11,'pbkdf2_sha256$24000$QDPpKwjBR16x$zvU5BAs+kdsI+3x1D5Tn7HtBu+DZBShb8DAGdcWdP9w=',NULL,0,'tuqibaga@yahoo.com','Blair','Jenkins','tuqibaga@yahoo.com',0,1,'2016-08-12 10:04:15.612934'),(12,'pbkdf2_sha256$24000$NADNGwnFQQOl$IIQ8Hk9SXhB9jFgELIsR5j7duMk8+1hYcpSwdfRXeb8=',NULL,0,'duloho@hotmail.com','Isadora','Guerra','duloho@hotmail.com',0,1,'2016-08-12 10:05:44.357777');
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_groups_user_id_94350c0c_uniq` (`user_id`,`group_id`),
  KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`),
  CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
INSERT INTO `auth_user_groups` VALUES (1,2,1);
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_user_permissions_user_id_14a6b632_uniq` (`user_id`,`permission_id`),
  KEY `auth_user_user_perm_permission_id_1fbb5f2c_fk_auth_permission_id` (`permission_id`),
  CONSTRAINT `auth_user_user_perm_permission_id_1fbb5f2c_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `authtoken_token`
--

DROP TABLE IF EXISTS `authtoken_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `authtoken_token` (
  `key` varchar(40) NOT NULL,
  `created` datetime(6) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`key`),
  UNIQUE KEY `user_id` (`user_id`),
  CONSTRAINT `authtoken_token_user_id_35299eff_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `authtoken_token`
--

LOCK TABLES `authtoken_token` WRITE;
/*!40000 ALTER TABLE `authtoken_token` DISABLE KEYS */;
/*!40000 ALTER TABLE `authtoken_token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin__content_type_id_c4bce8eb_fk_django_content_type_id` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`),
  CONSTRAINT `django_admin__content_type_id_c4bce8eb_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
INSERT INTO `django_admin_log` VALUES (1,'2016-08-12 08:47:56.241048','1','MERCHANTS',1,'Added.',3,1),(2,'2016-08-12 08:48:06.995330','2','CUSTOMER',1,'Added.',3,1),(3,'2016-08-12 08:48:31.438361','2','rodel@lagahit.com',1,'Added.',4,1),(4,'2016-08-12 08:48:45.742057','2','rodel@lagahit.com',2,'Changed first_name, last_name, email and groups.',4,1);
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (1,'admin','logentry'),(20,'apiv1','bankaccounts'),(23,'apiv1','feeds'),(22,'apiv1','myaccount'),(24,'apiv1','mysubscription'),(21,'apiv1','token'),(3,'auth','group'),(2,'auth','permission'),(4,'auth','user'),(7,'authtoken','token'),(5,'contenttypes','contenttype'),(8,'corsheaders','corsmodel'),(12,'front','branches'),(11,'front','company'),(16,'front','customer'),(10,'front','merchant'),(14,'front','package'),(13,'front','product'),(9,'front','profile'),(19,'front','profitsharing'),(17,'front','promotion'),(15,'front','services'),(18,'front','support'),(6,'sessions','session');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_migrations`
--

LOCK TABLES `django_migrations` WRITE;
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;
INSERT INTO `django_migrations` VALUES (1,'contenttypes','0001_initial','2016-08-12 08:46:21.124122'),(2,'auth','0001_initial','2016-08-12 08:46:21.557761'),(3,'admin','0001_initial','2016-08-12 08:46:21.646313'),(4,'admin','0002_logentry_remove_auto_add','2016-08-12 08:46:21.684788'),(5,'apiv1','0001_initial','2016-08-12 08:46:22.009850'),(6,'contenttypes','0002_remove_content_type_name','2016-08-12 08:46:22.165299'),(7,'auth','0002_alter_permission_name_max_length','2016-08-12 08:46:22.220301'),(8,'auth','0003_alter_user_email_max_length','2016-08-12 08:46:22.272894'),(9,'auth','0004_alter_user_username_opts','2016-08-12 08:46:22.296177'),(10,'auth','0005_alter_user_last_login_null','2016-08-12 08:46:22.352576'),(11,'auth','0006_require_contenttypes_0002','2016-08-12 08:46:22.356563'),(12,'auth','0007_alter_validators_add_error_messages','2016-08-12 08:46:22.379944'),(13,'authtoken','0001_initial','2016-08-12 08:46:22.466468'),(14,'front','0001_initial','2016-08-12 08:46:24.017121'),(15,'sessions','0001_initial','2016-08-12 08:46:24.056407'),(16,'front','0002_remove_customer_account_no','2016-08-12 10:05:30.577373');
/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_de54fa62` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
INSERT INTO `django_session` VALUES ('nfnlxzhrozpeuk4m0lacszsdscty7x0c','YzY4YjBlNWIxYTQxNTE5MzYzZmViODljYWViYmE3OTk0YjE5ZGM1YTp7Il9hdXRoX3VzZXJfaGFzaCI6IjQyNTQ0Mjc2MmNkM2Y5MjA4Y2Q4MmZjNjJkNDY5NWYwZDU4NTI4ZWEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIyIn0=','2016-08-26 08:49:37.939594');
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `front_branches`
--

DROP TABLE IF EXISTS `front_branches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `front_branches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime(6) NOT NULL,
  `updated` datetime(6) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `address` varchar(50) DEFAULT NULL,
  `latlong` varchar(50) DEFAULT NULL,
  `contact_no` varchar(50) DEFAULT NULL,
  `manager` varchar(150) DEFAULT NULL,
  `company_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `front_branches_447d3092` (`company_id`),
  CONSTRAINT `front_branches_company_id_9658bf0e_fk_front_company_id` FOREIGN KEY (`company_id`) REFERENCES `front_company` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `front_branches`
--

LOCK TABLES `front_branches` WRITE;
/*!40000 ALTER TABLE `front_branches` DISABLE KEYS */;
INSERT INTO `front_branches` VALUES (1,'2016-08-12 09:12:41.659729','2016-08-12 09:12:41.659818','Ursa Trevino','Avenue Street',NULL,'2345678','John Smith',1);
/*!40000 ALTER TABLE `front_branches` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `front_company`
--

DROP TABLE IF EXISTS `front_company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `front_company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime(6) NOT NULL,
  `updated` datetime(6) NOT NULL,
  `industry` varchar(50) DEFAULT NULL,
  `logo` varchar(100) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `address` varchar(50) DEFAULT NULL,
  `latlong` varchar(50) DEFAULT NULL,
  `contact_no` varchar(50) DEFAULT NULL,
  `authorized` varchar(50) DEFAULT NULL,
  `owner_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `owner_id` (`owner_id`),
  CONSTRAINT `front_company_owner_id_6ed33e11_fk_auth_user_id` FOREIGN KEY (`owner_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `front_company`
--

LOCK TABLES `front_company` WRITE;
/*!40000 ALTER TABLE `front_company` DISABLE KEYS */;
INSERT INTO `front_company` VALUES (1,'2016-08-12 08:50:24.775113','2016-08-12 08:50:24.775172','Food Industry',NULL,'Wall Street','Sesame Street Avenue',NULL,'234567','John Smith',2);
/*!40000 ALTER TABLE `front_company` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `front_customer`
--

DROP TABLE IF EXISTS `front_customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `front_customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` tinyint(1) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `profile_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `front_customer_5e7b1936` (`owner_id`),
  KEY `front_customer_83a0eb3f` (`profile_id`),
  CONSTRAINT `front_customer_owner_id_37ac655d_fk_auth_user_id` FOREIGN KEY (`owner_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `front_customer_profile_id_a507710d_fk_front_profile_id` FOREIGN KEY (`profile_id`) REFERENCES `front_profile` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `front_customer`
--

LOCK TABLES `front_customer` WRITE;
/*!40000 ALTER TABLE `front_customer` DISABLE KEYS */;
INSERT INTO `front_customer` VALUES (1,1,2,4);
/*!40000 ALTER TABLE `front_customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `front_customer_assign_package`
--

DROP TABLE IF EXISTS `front_customer_assign_package`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `front_customer_assign_package` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `front_customer_assign_package_customer_id_91f4a670_uniq` (`customer_id`,`package_id`),
  KEY `front_customer_assign_pa_package_id_7abee0fa_fk_front_package_id` (`package_id`),
  CONSTRAINT `front_customer_assign__customer_id_7dda4528_fk_front_customer_id` FOREIGN KEY (`customer_id`) REFERENCES `front_customer` (`id`),
  CONSTRAINT `front_customer_assign_pa_package_id_7abee0fa_fk_front_package_id` FOREIGN KEY (`package_id`) REFERENCES `front_package` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `front_customer_assign_package`
--

LOCK TABLES `front_customer_assign_package` WRITE;
/*!40000 ALTER TABLE `front_customer_assign_package` DISABLE KEYS */;
/*!40000 ALTER TABLE `front_customer_assign_package` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `front_merchant`
--

DROP TABLE IF EXISTS `front_merchant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `front_merchant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_status` varchar(50) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `front_merchant_user_id_ba7abd58_fk_auth_user_id` (`user_id`),
  CONSTRAINT `front_merchant_user_id_ba7abd58_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `front_merchant`
--

LOCK TABLES `front_merchant` WRITE;
/*!40000 ALTER TABLE `front_merchant` DISABLE KEYS */;
/*!40000 ALTER TABLE `front_merchant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `front_package`
--

DROP TABLE IF EXISTS `front_package`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `front_package` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime(6) NOT NULL,
  `updated` datetime(6) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `description` varchar(50) DEFAULT NULL,
  `exp_date` datetime(6) DEFAULT NULL,
  `price` double NOT NULL,
  `start_date` datetime(6) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `merchant_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `front_package_branch_id_7c8d7d19_fk_front_branches_id` (`branch_id`),
  KEY `front_package_merchant_id_b0b25541_fk_auth_user_id` (`merchant_id`),
  CONSTRAINT `front_package_branch_id_7c8d7d19_fk_front_branches_id` FOREIGN KEY (`branch_id`) REFERENCES `front_branches` (`id`),
  CONSTRAINT `front_package_merchant_id_b0b25541_fk_auth_user_id` FOREIGN KEY (`merchant_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `front_package`
--

LOCK TABLES `front_package` WRITE;
/*!40000 ALTER TABLE `front_package` DISABLE KEYS */;
INSERT INTO `front_package` VALUES (1,'2016-08-12 09:42:16.685062','2016-08-12 09:42:16.685120','Iris Glass','Saepe velit minima esse, tempore, fugiat ullam rer','2016-09-10 00:00:00.000000',929,'2016-08-27 00:00:00.000000',1,2),(2,'2016-08-12 09:45:55.094255','2016-08-12 09:45:55.094307','Colton Haney','Dolores ut et proident, ad dolore accusantium modi','2016-09-01 00:00:00.000000',642,'2016-08-24 00:00:00.000000',1,2);
/*!40000 ALTER TABLE `front_package` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `front_package_products`
--

DROP TABLE IF EXISTS `front_package_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `front_package_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `package_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `front_package_products_package_id_9dc51507_uniq` (`package_id`,`product_id`),
  KEY `front_package_products_product_id_7305ddae_fk_front_product_id` (`product_id`),
  CONSTRAINT `front_package_products_package_id_32de7d64_fk_front_package_id` FOREIGN KEY (`package_id`) REFERENCES `front_package` (`id`),
  CONSTRAINT `front_package_products_product_id_7305ddae_fk_front_product_id` FOREIGN KEY (`product_id`) REFERENCES `front_product` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `front_package_products`
--

LOCK TABLES `front_package_products` WRITE;
/*!40000 ALTER TABLE `front_package_products` DISABLE KEYS */;
INSERT INTO `front_package_products` VALUES (1,2,1);
/*!40000 ALTER TABLE `front_package_products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `front_product`
--

DROP TABLE IF EXISTS `front_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `front_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime(6) NOT NULL,
  `updated` datetime(6) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `price` double NOT NULL,
  `points` double NOT NULL,
  `description` longtext,
  `branch_id` int(11) DEFAULT NULL,
  `merchant_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `front_product_branch_id_31b7fb08_fk_front_branches_id` (`branch_id`),
  KEY `front_product_merchant_id_c7793a43_fk_auth_user_id` (`merchant_id`),
  CONSTRAINT `front_product_branch_id_31b7fb08_fk_front_branches_id` FOREIGN KEY (`branch_id`) REFERENCES `front_branches` (`id`),
  CONSTRAINT `front_product_merchant_id_c7793a43_fk_auth_user_id` FOREIGN KEY (`merchant_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `front_product`
--

LOCK TABLES `front_product` WRITE;
/*!40000 ALTER TABLE `front_product` DISABLE KEYS */;
INSERT INTO `front_product` VALUES (1,'2016-08-12 09:41:00.950667','2016-08-12 09:41:00.950715','BeanBag',1300,3,'Consectetur, quae vitae nobis vel exercitationem sunt, enim excepteur vel dolorem porro dolore.',1,2);
/*!40000 ALTER TABLE `front_product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `front_profile`
--

DROP TABLE IF EXISTS `front_profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `front_profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime(6) NOT NULL,
  `updated` datetime(6) NOT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `middle_name` varchar(50) DEFAULT NULL,
  `address` varchar(50) DEFAULT NULL,
  `contact_no` varchar(50) DEFAULT NULL,
  `province` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `email_add` varchar(50) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `front_profile_user_id_37fa6e22_fk_auth_user_id` (`user_id`),
  CONSTRAINT `front_profile_user_id_37fa6e22_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `front_profile`
--

LOCK TABLES `front_profile` WRITE;
/*!40000 ALTER TABLE `front_profile` DISABLE KEYS */;
INSERT INTO `front_profile` VALUES (3,'2016-08-12 10:04:15.789581','2016-08-12 10:04:15.789665','Blair','Jenkins',NULL,NULL,NULL,NULL,NULL,'tuqibaga@yahoo.com',2),(4,'2016-08-12 10:05:44.534127','2016-08-12 10:05:44.534213','Isadora','Guerra',NULL,NULL,NULL,NULL,NULL,'duloho@hotmail.com',2);
/*!40000 ALTER TABLE `front_profile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `front_profitsharing`
--

DROP TABLE IF EXISTS `front_profitsharing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `front_profitsharing` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime(6) NOT NULL,
  `updated` datetime(6) NOT NULL,
  `amount1` int(11) NOT NULL,
  `amount2` int(11) NOT NULL,
  `owner_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `front_profitsharing_owner_id_8f455756_fk_auth_user_id` (`owner_id`),
  CONSTRAINT `front_profitsharing_owner_id_8f455756_fk_auth_user_id` FOREIGN KEY (`owner_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `front_profitsharing`
--

LOCK TABLES `front_profitsharing` WRITE;
/*!40000 ALTER TABLE `front_profitsharing` DISABLE KEYS */;
INSERT INTO `front_profitsharing` VALUES (1,'2016-08-12 09:33:13.863211','2016-08-12 09:33:13.863256',8,9,2);
/*!40000 ALTER TABLE `front_profitsharing` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `front_promotion`
--

DROP TABLE IF EXISTS `front_promotion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `front_promotion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime(6) NOT NULL,
  `updated` datetime(6) NOT NULL,
  `title` varchar(50) DEFAULT NULL,
  `description` varchar(50) DEFAULT NULL,
  `date_to` datetime(6) NOT NULL,
  `date_from` datetime(6) NOT NULL,
  `owner_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `front_promotion_owner_id_e1bfe7aa_fk_auth_user_id` (`owner_id`),
  CONSTRAINT `front_promotion_owner_id_e1bfe7aa_fk_auth_user_id` FOREIGN KEY (`owner_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `front_promotion`
--

LOCK TABLES `front_promotion` WRITE;
/*!40000 ALTER TABLE `front_promotion` DISABLE KEYS */;
INSERT INTO `front_promotion` VALUES (1,'2016-08-12 09:33:54.984257','2016-08-12 09:33:54.984314','Christmas','Christmas Eve','2016-09-09 00:00:00.000000','2016-08-26 00:00:00.000000',2);
/*!40000 ALTER TABLE `front_promotion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `front_promotion_branch`
--

DROP TABLE IF EXISTS `front_promotion_branch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `front_promotion_branch` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `promotion_id` int(11) NOT NULL,
  `branches_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `front_promotion_branch_promotion_id_6387eeb1_uniq` (`promotion_id`,`branches_id`),
  KEY `front_promotion_branch_branches_id_6de7307d_fk_front_branches_id` (`branches_id`),
  CONSTRAINT `front_promotion_bran_promotion_id_0817786e_fk_front_promotion_id` FOREIGN KEY (`promotion_id`) REFERENCES `front_promotion` (`id`),
  CONSTRAINT `front_promotion_branch_branches_id_6de7307d_fk_front_branches_id` FOREIGN KEY (`branches_id`) REFERENCES `front_branches` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `front_promotion_branch`
--

LOCK TABLES `front_promotion_branch` WRITE;
/*!40000 ALTER TABLE `front_promotion_branch` DISABLE KEYS */;
INSERT INTO `front_promotion_branch` VALUES (1,1,1);
/*!40000 ALTER TABLE `front_promotion_branch` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `front_services`
--

DROP TABLE IF EXISTS `front_services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `front_services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime(6) NOT NULL,
  `updated` datetime(6) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `description` varchar(50) DEFAULT NULL,
  `price` int(11) NOT NULL,
  `points` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `front_services_branch_id_0e1f9633_fk_front_branches_id` (`branch_id`),
  CONSTRAINT `front_services_branch_id_0e1f9633_fk_front_branches_id` FOREIGN KEY (`branch_id`) REFERENCES `front_branches` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `front_services`
--

LOCK TABLES `front_services` WRITE;
/*!40000 ALTER TABLE `front_services` DISABLE KEYS */;
/*!40000 ALTER TABLE `front_services` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `front_support`
--

DROP TABLE IF EXISTS `front_support`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `front_support` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime(6) NOT NULL,
  `updated` datetime(6) NOT NULL,
  `complain` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `front_support`
--

LOCK TABLES `front_support` WRITE;
/*!40000 ALTER TABLE `front_support` DISABLE KEYS */;
/*!40000 ALTER TABLE `front_support` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `front_support_branch`
--

DROP TABLE IF EXISTS `front_support_branch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `front_support_branch` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `support_id` int(11) NOT NULL,
  `branches_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `front_support_branch_support_id_6631d9fd_uniq` (`support_id`,`branches_id`),
  KEY `front_support_branch_branches_id_323cf1e1_fk_front_branches_id` (`branches_id`),
  CONSTRAINT `front_support_branch_branches_id_323cf1e1_fk_front_branches_id` FOREIGN KEY (`branches_id`) REFERENCES `front_branches` (`id`),
  CONSTRAINT `front_support_branch_support_id_748d8f98_fk_front_support_id` FOREIGN KEY (`support_id`) REFERENCES `front_support` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `front_support_branch`
--

LOCK TABLES `front_support_branch` WRITE;
/*!40000 ALTER TABLE `front_support_branch` DISABLE KEYS */;
/*!40000 ALTER TABLE `front_support_branch` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-08-12 18:09:08
