-- MySQL dump 10.13  Distrib 5.6.20, for osx10.9 (x86_64)
--
-- Host: localhost    Database: lifestyle
-- ------------------------------------------------------
-- Server version	5.6.14

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `apiv1_bankaccounts`
--

DROP TABLE IF EXISTS `apiv1_bankaccounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apiv1_bankaccounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime(6) NOT NULL,
  `updated` datetime(6) NOT NULL,
  `refid` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`),
  CONSTRAINT `apiv1_bankaccounts_user_id_b57f8af0_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apiv1_bankaccounts`
--

LOCK TABLES `apiv1_bankaccounts` WRITE;
/*!40000 ALTER TABLE `apiv1_bankaccounts` DISABLE KEYS */;
/*!40000 ALTER TABLE `apiv1_bankaccounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apiv1_feeds`
--

DROP TABLE IF EXISTS `apiv1_feeds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apiv1_feeds` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime(6) NOT NULL,
  `updated` datetime(6) NOT NULL,
  `content` longtext NOT NULL,
  `tag` varchar(200) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `apiv1_feeds_user_id_2ff7f0f8_fk_auth_user_id` (`user_id`),
  CONSTRAINT `apiv1_feeds_user_id_2ff7f0f8_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apiv1_feeds`
--

LOCK TABLES `apiv1_feeds` WRITE;
/*!40000 ALTER TABLE `apiv1_feeds` DISABLE KEYS */;
/*!40000 ALTER TABLE `apiv1_feeds` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apiv1_myaccount`
--

DROP TABLE IF EXISTS `apiv1_myaccount`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apiv1_myaccount` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime(6) NOT NULL,
  `updated` datetime(6) NOT NULL,
  `balance` double NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`),
  CONSTRAINT `apiv1_myaccount_user_id_af526d40_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apiv1_myaccount`
--

LOCK TABLES `apiv1_myaccount` WRITE;
/*!40000 ALTER TABLE `apiv1_myaccount` DISABLE KEYS */;
/*!40000 ALTER TABLE `apiv1_myaccount` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apiv1_mysubscription`
--

DROP TABLE IF EXISTS `apiv1_mysubscription`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apiv1_mysubscription` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime(6) NOT NULL,
  `updated` datetime(6) NOT NULL,
  `merchant_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `apiv1_mysubscription_merchant_id_67e41579_fk_auth_user_id` (`merchant_id`),
  KEY `apiv1_mysubscription_user_id_beb6a6c2_fk_auth_user_id` (`user_id`),
  CONSTRAINT `apiv1_mysubscription_user_id_beb6a6c2_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `apiv1_mysubscription_merchant_id_67e41579_fk_auth_user_id` FOREIGN KEY (`merchant_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apiv1_mysubscription`
--

LOCK TABLES `apiv1_mysubscription` WRITE;
/*!40000 ALTER TABLE `apiv1_mysubscription` DISABLE KEYS */;
/*!40000 ALTER TABLE `apiv1_mysubscription` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apiv1_token`
--

DROP TABLE IF EXISTS `apiv1_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apiv1_token` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime(6) NOT NULL,
  `updated` datetime(6) NOT NULL,
  `key` varchar(200) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`),
  CONSTRAINT `apiv1_token_user_id_b31f8b25_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apiv1_token`
--

LOCK TABLES `apiv1_token` WRITE;
/*!40000 ALTER TABLE `apiv1_token` DISABLE KEYS */;
/*!40000 ALTER TABLE `apiv1_token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
INSERT INTO `auth_group` VALUES (2,'CUSTOMER'),(1,'MERCHANTS');
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissi_permission_id_84c5c92e_fk_auth_permission_id` (`permission_id`),
  CONSTRAINT `auth_group_permissi_permission_id_84c5c92e_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_01ab375a_uniq` (`content_type_id`,`codename`),
  CONSTRAINT `auth_permissi_content_type_id_2f476e4b_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add log entry',1,'add_logentry'),(2,'Can change log entry',1,'change_logentry'),(3,'Can delete log entry',1,'delete_logentry'),(4,'Can add permission',2,'add_permission'),(5,'Can change permission',2,'change_permission'),(6,'Can delete permission',2,'delete_permission'),(7,'Can add group',3,'add_group'),(8,'Can change group',3,'change_group'),(9,'Can delete group',3,'delete_group'),(10,'Can add user',4,'add_user'),(11,'Can change user',4,'change_user'),(12,'Can delete user',4,'delete_user'),(13,'Can add content type',5,'add_contenttype'),(14,'Can change content type',5,'change_contenttype'),(15,'Can delete content type',5,'delete_contenttype'),(16,'Can add session',6,'add_session'),(17,'Can change session',6,'change_session'),(18,'Can delete session',6,'delete_session'),(19,'Can add token',7,'add_token'),(20,'Can change token',7,'change_token'),(21,'Can delete token',7,'delete_token'),(22,'Can add cors model',8,'add_corsmodel'),(23,'Can change cors model',8,'change_corsmodel'),(24,'Can delete cors model',8,'delete_corsmodel'),(25,'Can add profile',9,'add_profile'),(26,'Can change profile',9,'change_profile'),(27,'Can delete profile',9,'delete_profile'),(28,'Can add merchant',10,'add_merchant'),(29,'Can change merchant',10,'change_merchant'),(30,'Can delete merchant',10,'delete_merchant'),(31,'Can add company',11,'add_company'),(32,'Can change company',11,'change_company'),(33,'Can delete company',11,'delete_company'),(34,'Can add branches',12,'add_branches'),(35,'Can change branches',12,'change_branches'),(36,'Can delete branches',12,'delete_branches'),(37,'Can add product',13,'add_product'),(38,'Can change product',13,'change_product'),(39,'Can delete product',13,'delete_product'),(40,'Can add package',14,'add_package'),(41,'Can change package',14,'change_package'),(42,'Can delete package',14,'delete_package'),(43,'Can add services',15,'add_services'),(44,'Can change services',15,'change_services'),(45,'Can delete services',15,'delete_services'),(46,'Can add customer',16,'add_customer'),(47,'Can change customer',16,'change_customer'),(48,'Can delete customer',16,'delete_customer'),(49,'Can add promotion',17,'add_promotion'),(50,'Can change promotion',17,'change_promotion'),(51,'Can delete promotion',17,'delete_promotion'),(52,'Can add support',18,'add_support'),(53,'Can change support',18,'change_support'),(54,'Can delete support',18,'delete_support'),(55,'Can add profit sharing',19,'add_profitsharing'),(56,'Can change profit sharing',19,'change_profitsharing'),(57,'Can delete profit sharing',19,'delete_profitsharing'),(58,'Can add bank accounts',20,'add_bankaccounts'),(59,'Can change bank accounts',20,'change_bankaccounts'),(60,'Can delete bank accounts',20,'delete_bankaccounts'),(61,'Can add token',21,'add_token'),(62,'Can change token',21,'change_token'),(63,'Can delete token',21,'delete_token'),(64,'Can add my account',22,'add_myaccount'),(65,'Can change my account',22,'change_myaccount'),(66,'Can delete my account',22,'delete_myaccount'),(67,'Can add feeds',23,'add_feeds'),(68,'Can change feeds',23,'change_feeds'),(69,'Can delete feeds',23,'delete_feeds'),(70,'Can add my subscription',24,'add_mysubscription'),(71,'Can change my subscription',24,'change_mysubscription'),(72,'Can delete my subscription',24,'delete_mysubscription');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(30) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
INSERT INTO `auth_user` VALUES (1,'pbkdf2_sha256$24000$nvvpM3IcVoNm$xbCu3/Pa3aRMWH7gOpwHbujH5i9adsnMew6gWbJfLLI=','2016-08-07 02:58:46.883113',1,'administrator','','','cnard1511@gmail.com',1,1,'2016-08-06 05:26:50.347578'),(10,'pbkdf2_sha256$24000$YZoDsbo7b0G9$RaB4T6h+teAWIagAF2+vkQbs0HyUHZfD9V01aWKyIl8=','2016-08-07 03:02:16.192167',0,'rodelio','Rodel','Lagahit','rodel@lagahit.com',0,1,'2016-08-07 03:00:22.000000'),(11,'pbkdf2_sha256$24000$JtYeeuALf2Lp$mO1d2w3cBoZjgOMQ09/DfhPNDuf8h/pzu8+61DTS1dg=',NULL,0,'abc123@gmail.com','abc','123','abc123@gmail.com',0,1,'2016-08-07 03:02:44.436311');
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_groups_user_id_94350c0c_uniq` (`user_id`,`group_id`),
  KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`),
  CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
INSERT INTO `auth_user_groups` VALUES (1,10,1);
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_user_permissions_user_id_14a6b632_uniq` (`user_id`,`permission_id`),
  KEY `auth_user_user_perm_permission_id_1fbb5f2c_fk_auth_permission_id` (`permission_id`),
  CONSTRAINT `auth_user_user_perm_permission_id_1fbb5f2c_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `authtoken_token`
--

DROP TABLE IF EXISTS `authtoken_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `authtoken_token` (
  `key` varchar(40) NOT NULL,
  `created` datetime(6) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`key`),
  UNIQUE KEY `user_id` (`user_id`),
  CONSTRAINT `authtoken_token_user_id_35299eff_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `authtoken_token`
--

LOCK TABLES `authtoken_token` WRITE;
/*!40000 ALTER TABLE `authtoken_token` DISABLE KEYS */;
/*!40000 ALTER TABLE `authtoken_token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin__content_type_id_c4bce8eb_fk_django_content_type_id` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`),
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `django_admin__content_type_id_c4bce8eb_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
INSERT INTO `django_admin_log` VALUES (1,'2016-08-07 02:59:13.363006','3','beta@test.com',3,'',4,1),(2,'2016-08-07 02:59:13.365061','9','beta@test.come',3,'',4,1),(3,'2016-08-07 02:59:13.367263','6','beta@test.comp',3,'',4,1),(4,'2016-08-07 02:59:13.371073','8','beta@test.comR',3,'',4,1),(5,'2016-08-07 02:59:13.372443','4','beta@test.coms',3,'',4,1),(6,'2016-08-07 02:59:13.373914','7','beta@test.comw',3,'',4,1),(7,'2016-08-07 02:59:13.375341','5','beta@test.comz',3,'',4,1),(8,'2016-08-07 02:59:13.376585','2','rodelio',3,'',4,1),(9,'2016-08-07 02:59:21.881444','1','MERCHANTS',1,'Added.',3,1),(10,'2016-08-07 02:59:27.420012','2','CUSTOMER',1,'Added.',3,1),(11,'2016-08-07 03:00:22.584835','10','rodelio',1,'Added.',4,1),(12,'2016-08-07 03:00:38.896083','10','rodelio',2,'Changed first_name, last_name, email and groups.',4,1),(13,'2016-08-07 03:01:26.462535','2','Company object',1,'Added.',11,1),(14,'2016-08-07 03:02:04.787429','1','Merchant object',1,'Added.',10,1);
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (1,'admin','logentry'),(20,'apiv1','bankaccounts'),(23,'apiv1','feeds'),(22,'apiv1','myaccount'),(24,'apiv1','mysubscription'),(21,'apiv1','token'),(3,'auth','group'),(2,'auth','permission'),(4,'auth','user'),(7,'authtoken','token'),(5,'contenttypes','contenttype'),(8,'corsheaders','corsmodel'),(12,'main','branches'),(11,'main','company'),(16,'main','customer'),(10,'main','merchant'),(14,'main','package'),(13,'main','product'),(9,'main','profile'),(19,'main','profitsharing'),(17,'main','promotion'),(15,'main','services'),(18,'main','support'),(6,'sessions','session');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_migrations`
--

LOCK TABLES `django_migrations` WRITE;
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;
INSERT INTO `django_migrations` VALUES (1,'contenttypes','0001_initial','2016-08-07 02:56:47.765275'),(2,'auth','0001_initial','2016-08-07 02:56:48.066306'),(3,'admin','0001_initial','2016-08-07 02:56:48.127923'),(4,'admin','0002_logentry_remove_auto_add','2016-08-07 02:56:48.155313'),(5,'apiv1','0001_initial','2016-08-07 02:56:48.450054'),(6,'contenttypes','0002_remove_content_type_name','2016-08-07 02:56:48.541304'),(7,'auth','0002_alter_permission_name_max_length','2016-08-07 02:56:48.582328'),(8,'auth','0003_alter_user_email_max_length','2016-08-07 02:56:48.621821'),(9,'auth','0004_alter_user_username_opts','2016-08-07 02:56:48.642975'),(10,'auth','0005_alter_user_last_login_null','2016-08-07 02:56:48.685836'),(11,'auth','0006_require_contenttypes_0002','2016-08-07 02:56:48.688779'),(12,'auth','0007_alter_validators_add_error_messages','2016-08-07 02:56:48.708361'),(13,'authtoken','0001_initial','2016-08-07 02:56:48.764343'),(14,'main','0001_initial','2016-08-07 02:56:50.048769'),(15,'sessions','0001_initial','2016-08-07 02:56:50.082205');
/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_de54fa62` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
INSERT INTO `django_session` VALUES ('knw76lroq5oa7p51lo2o3n6rcn4i9vz5','ODMxYmQ1NDQxZjliZjExY2JkYmY4N2ZiMTc4NGMyZGU3MzNkMDdjNTp7Il9hdXRoX3VzZXJfaGFzaCI6ImQzN2M2MmNkOTU5ZGE3ZGI1MDIzMjNkMDJkY2IzZmU0ZDU1NjFhZDUiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxMCJ9','2016-08-21 03:02:16.196044');
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `main_branches`
--

DROP TABLE IF EXISTS `main_branches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `main_branches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime(6) NOT NULL,
  `updated` datetime(6) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `location` varchar(50) DEFAULT NULL,
  `contact_no` varchar(50) DEFAULT NULL,
  `manager` varchar(150) DEFAULT NULL,
  `company_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `main_branches_447d3092` (`company_id`),
  CONSTRAINT `main_branches_company_id_777fc0d6_fk_main_company_id` FOREIGN KEY (`company_id`) REFERENCES `main_company` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `main_branches`
--

LOCK TABLES `main_branches` WRITE;
/*!40000 ALTER TABLE `main_branches` DISABLE KEYS */;
INSERT INTO `main_branches` VALUES (1,'2016-08-07 03:03:10.142086','2016-08-07 03:03:10.142156','Branch 1','Somwhere down the road','09876543','Me',2),(2,'2016-08-07 03:03:25.653567','2016-08-07 03:03:25.653635','Branch 2','ALong the road','23456789','You',2);
/*!40000 ALTER TABLE `main_branches` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `main_company`
--

DROP TABLE IF EXISTS `main_company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `main_company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `industry` varchar(50) DEFAULT NULL,
  `logo` varchar(100) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `address` varchar(50) DEFAULT NULL,
  `latlong` varchar(50) DEFAULT NULL,
  `contact_no` varchar(50) DEFAULT NULL,
  `authorized` varchar(50) DEFAULT NULL,
  `reference_no` varchar(50) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`),
  CONSTRAINT `main_company_user_id_ed1c41ce_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `main_company`
--

LOCK TABLES `main_company` WRITE;
/*!40000 ALTER TABLE `main_company` DISABLE KEYS */;
INSERT INTO `main_company` VALUES (2,'Hotels','wasad','GOGO','Somewhere along the road','1233213, 1231231','09876543','Rodel','1234567890asd',10);
/*!40000 ALTER TABLE `main_company` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `main_customer`
--

DROP TABLE IF EXISTS `main_customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `main_customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` tinyint(1) NOT NULL,
  `account_no` int(11) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `profile_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `main_customer_5e7b1936` (`owner_id`),
  KEY `main_customer_83a0eb3f` (`profile_id`),
  CONSTRAINT `main_customer_profile_id_89e03af9_fk_main_profile_id` FOREIGN KEY (`profile_id`) REFERENCES `main_profile` (`id`),
  CONSTRAINT `main_customer_owner_id_9483dd8e_fk_auth_user_id` FOREIGN KEY (`owner_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `main_customer`
--

LOCK TABLES `main_customer` WRITE;
/*!40000 ALTER TABLE `main_customer` DISABLE KEYS */;
INSERT INTO `main_customer` VALUES (1,1,123456789,10,1);
/*!40000 ALTER TABLE `main_customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `main_customer_assign_package`
--

DROP TABLE IF EXISTS `main_customer_assign_package`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `main_customer_assign_package` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `main_customer_assign_package_customer_id_dd97980d_uniq` (`customer_id`,`package_id`),
  KEY `main_customer_assign_pack_package_id_8af3110e_fk_main_package_id` (`package_id`),
  CONSTRAINT `main_customer_assign_pack_package_id_8af3110e_fk_main_package_id` FOREIGN KEY (`package_id`) REFERENCES `main_package` (`id`),
  CONSTRAINT `main_customer_assign_pa_customer_id_c785f595_fk_main_customer_id` FOREIGN KEY (`customer_id`) REFERENCES `main_customer` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `main_customer_assign_package`
--

LOCK TABLES `main_customer_assign_package` WRITE;
/*!40000 ALTER TABLE `main_customer_assign_package` DISABLE KEYS */;
/*!40000 ALTER TABLE `main_customer_assign_package` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `main_merchant`
--

DROP TABLE IF EXISTS `main_merchant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `main_merchant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_status` varchar(50) DEFAULT NULL,
  `bank` varchar(100) DEFAULT NULL,
  `reference_no` varchar(100) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `main_merchant_user_id_8aa11416_fk_auth_user_id` (`user_id`),
  CONSTRAINT `main_merchant_user_id_8aa11416_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `main_merchant`
--

LOCK TABLES `main_merchant` WRITE;
/*!40000 ALTER TABLE `main_merchant` DISABLE KEYS */;
INSERT INTO `main_merchant` VALUES (1,'10000','Unionbank','098765432asd',10);
/*!40000 ALTER TABLE `main_merchant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `main_package`
--

DROP TABLE IF EXISTS `main_package`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `main_package` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime(6) NOT NULL,
  `updated` datetime(6) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `description` varchar(50) DEFAULT NULL,
  `exp_date` datetime(6) DEFAULT NULL,
  `price` double NOT NULL,
  `start_date` datetime(6) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `merchant_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `main_package_branch_id_a1ddbc1d_fk_main_branches_id` (`branch_id`),
  KEY `main_package_merchant_id_53084e4e_fk_auth_user_id` (`merchant_id`),
  CONSTRAINT `main_package_merchant_id_53084e4e_fk_auth_user_id` FOREIGN KEY (`merchant_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `main_package_branch_id_a1ddbc1d_fk_main_branches_id` FOREIGN KEY (`branch_id`) REFERENCES `main_branches` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `main_package`
--

LOCK TABLES `main_package` WRITE;
/*!40000 ALTER TABLE `main_package` DISABLE KEYS */;
INSERT INTO `main_package` VALUES (1,'2016-08-07 03:04:41.340862','2016-08-07 03:04:41.340915','Package 101','High blood to the max','2016-08-31 00:00:00.000000',600,'2016-08-01 00:00:00.000000',1,10);
/*!40000 ALTER TABLE `main_package` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `main_package_products`
--

DROP TABLE IF EXISTS `main_package_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `main_package_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `package_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `main_package_products_package_id_d81faccb_uniq` (`package_id`,`product_id`),
  KEY `main_package_products_product_id_179e33b4_fk_main_product_id` (`product_id`),
  CONSTRAINT `main_package_products_product_id_179e33b4_fk_main_product_id` FOREIGN KEY (`product_id`) REFERENCES `main_product` (`id`),
  CONSTRAINT `main_package_products_package_id_11e6f869_fk_main_package_id` FOREIGN KEY (`package_id`) REFERENCES `main_package` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `main_package_products`
--

LOCK TABLES `main_package_products` WRITE;
/*!40000 ALTER TABLE `main_package_products` DISABLE KEYS */;
INSERT INTO `main_package_products` VALUES (1,1,1),(2,1,2);
/*!40000 ALTER TABLE `main_package_products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `main_product`
--

DROP TABLE IF EXISTS `main_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `main_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime(6) NOT NULL,
  `updated` datetime(6) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `price` double NOT NULL,
  `points` double NOT NULL,
  `description` longtext,
  `branch_id` int(11) DEFAULT NULL,
  `merchant_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `main_product_branch_id_01580bba_fk_main_branches_id` (`branch_id`),
  KEY `main_product_merchant_id_52aeed5b_fk_auth_user_id` (`merchant_id`),
  CONSTRAINT `main_product_merchant_id_52aeed5b_fk_auth_user_id` FOREIGN KEY (`merchant_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `main_product_branch_id_01580bba_fk_main_branches_id` FOREIGN KEY (`branch_id`) REFERENCES `main_branches` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `main_product`
--

LOCK TABLES `main_product` WRITE;
/*!40000 ALTER TABLE `main_product` DISABLE KEYS */;
INSERT INTO `main_product` VALUES (1,'2016-08-07 03:03:42.406580','2016-08-07 03:03:42.406637','Pochero',400,20,'Baka',1,10),(2,'2016-08-07 03:03:59.639973','2016-08-07 03:03:59.640027','Kaldereta',200,10,'Orange nga sabaw',2,10);
/*!40000 ALTER TABLE `main_product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `main_profile`
--

DROP TABLE IF EXISTS `main_profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `main_profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `middle_name` varchar(50) DEFAULT NULL,
  `gender` varchar(50) DEFAULT NULL,
  `civil_status` varchar(50) DEFAULT NULL,
  `address` varchar(50) DEFAULT NULL,
  `contact_no` varchar(50) DEFAULT NULL,
  `province` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `email_add` varchar(50) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `main_profile_user_id_b40d720a_fk_auth_user_id` (`user_id`),
  CONSTRAINT `main_profile_user_id_b40d720a_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `main_profile`
--

LOCK TABLES `main_profile` WRITE;
/*!40000 ALTER TABLE `main_profile` DISABLE KEYS */;
INSERT INTO `main_profile` VALUES (1,'abc','123',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'abc123@gmail.com',10);
/*!40000 ALTER TABLE `main_profile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `main_profitsharing`
--

DROP TABLE IF EXISTS `main_profitsharing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `main_profitsharing` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime(6) NOT NULL,
  `updated` datetime(6) NOT NULL,
  `amount1` int(11) NOT NULL,
  `amount2` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `main_profitsharing`
--

LOCK TABLES `main_profitsharing` WRITE;
/*!40000 ALTER TABLE `main_profitsharing` DISABLE KEYS */;
/*!40000 ALTER TABLE `main_profitsharing` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `main_profitsharing_company`
--

DROP TABLE IF EXISTS `main_profitsharing_company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `main_profitsharing_company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `profitsharing_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `main_profitsharing_company_profitsharing_id_d3e21358_uniq` (`profitsharing_id`,`company_id`),
  KEY `main_profitsharing_compan_company_id_a3d20e02_fk_main_company_id` (`company_id`),
  CONSTRAINT `main_profitsharing_compan_company_id_a3d20e02_fk_main_company_id` FOREIGN KEY (`company_id`) REFERENCES `main_company` (`id`),
  CONSTRAINT `main_profitsh_profitsharing_id_b0fffd8b_fk_main_profitsharing_id` FOREIGN KEY (`profitsharing_id`) REFERENCES `main_profitsharing` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `main_profitsharing_company`
--

LOCK TABLES `main_profitsharing_company` WRITE;
/*!40000 ALTER TABLE `main_profitsharing_company` DISABLE KEYS */;
/*!40000 ALTER TABLE `main_profitsharing_company` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `main_promotion`
--

DROP TABLE IF EXISTS `main_promotion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `main_promotion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime(6) NOT NULL,
  `updated` datetime(6) NOT NULL,
  `title` varchar(50) DEFAULT NULL,
  `description` varchar(50) DEFAULT NULL,
  `date_to` datetime(6) NOT NULL,
  `date_from` datetime(6) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `main_promotion_user_id_3d66db33_fk_auth_user_id` (`user_id`),
  CONSTRAINT `main_promotion_user_id_3d66db33_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `main_promotion`
--

LOCK TABLES `main_promotion` WRITE;
/*!40000 ALTER TABLE `main_promotion` DISABLE KEYS */;
/*!40000 ALTER TABLE `main_promotion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `main_promotion_branch`
--

DROP TABLE IF EXISTS `main_promotion_branch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `main_promotion_branch` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `promotion_id` int(11) NOT NULL,
  `branches_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `main_promotion_branch_promotion_id_89c12b50_uniq` (`promotion_id`,`branches_id`),
  KEY `main_promotion_branch_branches_id_9532ba3a_fk_main_branches_id` (`branches_id`),
  CONSTRAINT `main_promotion_branch_branches_id_9532ba3a_fk_main_branches_id` FOREIGN KEY (`branches_id`) REFERENCES `main_branches` (`id`),
  CONSTRAINT `main_promotion_branch_promotion_id_9268bcb6_fk_main_promotion_id` FOREIGN KEY (`promotion_id`) REFERENCES `main_promotion` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `main_promotion_branch`
--

LOCK TABLES `main_promotion_branch` WRITE;
/*!40000 ALTER TABLE `main_promotion_branch` DISABLE KEYS */;
/*!40000 ALTER TABLE `main_promotion_branch` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `main_services`
--

DROP TABLE IF EXISTS `main_services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `main_services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime(6) NOT NULL,
  `updated` datetime(6) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `description` varchar(50) DEFAULT NULL,
  `price` int(11) NOT NULL,
  `points` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `main_services_branch_id_9193ae01_fk_main_branches_id` (`branch_id`),
  CONSTRAINT `main_services_branch_id_9193ae01_fk_main_branches_id` FOREIGN KEY (`branch_id`) REFERENCES `main_branches` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `main_services`
--

LOCK TABLES `main_services` WRITE;
/*!40000 ALTER TABLE `main_services` DISABLE KEYS */;
/*!40000 ALTER TABLE `main_services` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `main_support`
--

DROP TABLE IF EXISTS `main_support`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `main_support` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime(6) NOT NULL,
  `updated` datetime(6) NOT NULL,
  `complain` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `main_support`
--

LOCK TABLES `main_support` WRITE;
/*!40000 ALTER TABLE `main_support` DISABLE KEYS */;
/*!40000 ALTER TABLE `main_support` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `main_support_branch`
--

DROP TABLE IF EXISTS `main_support_branch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `main_support_branch` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `support_id` int(11) NOT NULL,
  `branches_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `main_support_branch_support_id_5f08881c_uniq` (`support_id`,`branches_id`),
  KEY `main_support_branch_branches_id_61f3fbd8_fk_main_branches_id` (`branches_id`),
  CONSTRAINT `main_support_branch_branches_id_61f3fbd8_fk_main_branches_id` FOREIGN KEY (`branches_id`) REFERENCES `main_branches` (`id`),
  CONSTRAINT `main_support_branch_support_id_bbc91b5a_fk_main_support_id` FOREIGN KEY (`support_id`) REFERENCES `main_support` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `main_support_branch`
--

LOCK TABLES `main_support_branch` WRITE;
/*!40000 ALTER TABLE `main_support_branch` DISABLE KEYS */;
/*!40000 ALTER TABLE `main_support_branch` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-08-07 11:11:59
